package com.ibghub.search.listing.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ibghub.search.listing.R;

/**
 * Created by Lenovo on 18-Sep-16.
 */
public class PagerAdapterImageSlider extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int[] images;

    public PagerAdapterImageSlider(Context mContext, int[] images) {
        this.mContext = mContext;
        this.images = images;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgflag;

        View itemView = mLayoutInflater.inflate(R.layout.adapter_viewpager_item, container,
                false);

        imgflag = (ImageView) itemView.findViewById(R.id.iv_member_banner_images);
        imgflag.setImageResource(images[position]);
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
