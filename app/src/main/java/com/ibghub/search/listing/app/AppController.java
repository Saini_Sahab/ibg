package com.ibghub.search.listing.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.volly.LruBitmapCache;


public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class.getSimpleName();
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        //handler = new Handler();
        //uiListeners = new HashMap<>();

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        TypedArray tableClasses = getResources().obtainTypedArray(
                R.array.tables);
        for (int index = 0; index < tableClasses.length(); index++)
            try {
                Class.forName(tableClasses.getString(index));

            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        tableClasses.recycle();

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    /*public void addUIListener(Class cls,UiListener listener) {
        if(getOrCreateUIListeners(cls).contains(listener)) {
            getOrCreateUIListeners(cls).add(listener);
        }
    }

    @SuppressWarnings("unchecked")
    private Collection getOrCreateUIListeners(Class cls) {
        Collection collection = (Collection) uiListeners.get(cls);
        if (collection == null) {
            collection = new ArrayList();
            uiListeners.put(cls, collection);
        }
        return collection;
    }

    private Map<Class, Collection> uiListeners;
    private Handler handler;

    public void runOnUiThread(final Runnable runnable) {
//Log.d("Demo ","Exception Area");
        handler.post(runnable);
    }

    public Collection getUIListeners(Class cls) {
        return Collections.unmodifiableCollection(getOrCreateUIListeners(cls));
    }*/

    /*public void onMessageReceived(final String type, final Object object) {
        AppController.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (UiListener onMessageReceiveListener :
                        AppController.getInstance().getUIListeners(AppController.this))
                    onMessageReceiveListener.update(type, object);
            }
        });
    }*/


    /*private Map<Class, Collection> uiListeners;
    private Handler handler;

    public <T extends Object> void addUIListener(Class<T> cls,
                                                 T listener) {
        if (!getOrCreateUIListeners(cls).contains(listener)) {
            getOrCreateUIListeners(cls).add(listener);
        }
        //Toast.makeText(getApplicationContext(),"OnResume",Toast.LENGTH_SHORT).show();
        //SetOnlineOfflineStatus.setStatus(getApplicationContext());
    }

    public <T extends Object> void removeUIListener(Class<T> cls,
                                                 T listener) {
        if (getOrCreateUIListeners(cls).contains(listener)) {
            getOrCreateUIListeners(cls).remove(listener);
        }
        //Toast.makeText(getApplicationContext(),"OnResume",Toast.LENGTH_SHORT).show();
        //SetOnlineOfflineStatus.setStatus(getApplicationContext());
    }


    @SuppressWarnings("unchecked")
    private <T extends Object> Collection<T> getOrCreateUIListeners(
            Class<T> cls) {
        Collection<T> collection = (Collection<T>) uiListeners.get(cls);
        if (collection == null) {
            collection = new ArrayList<T>();
            uiListeners.put(cls, collection);
        }
        return collection;
    }

    public void runOnUiThread(final Runnable runnable) {
        //Log.d("Demo ","Exception Area");
        handler.post(runnable);
    }

    public <T extends Object> Collection<T> getUIListeners(Class<T> cls) {

        return Collections.unmodifiableCollection(getOrCreateUIListeners(cls));
    }*/


}
