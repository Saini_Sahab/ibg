package com.ibghub.search.listing.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Preferences;
import com.ibghub.search.listing.utility.Utils;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.ibghub.search.listing.utility.Constants.USER_ID;


public class LoginAsIbgUserActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText emailEditText, passwordEditText;
    private Button loginButton;
    private Context context;
    private ProgressBar progressBar;
    private RelativeLayout loginAsUserRelativeLayout;
    private Intent intent;
    RelativeLayout rlMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFullScreen(this);
        setContentView(R.layout.activity_login_as_user);
        context = this;
        bind();
    }


    private void bind() {
        emailEditText = (EditText) findViewById(R.id.et_email);
        passwordEditText = (EditText) findViewById(R.id.et_password);
        loginButton = (Button) findViewById(R.id.login_as_ibguser);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        loginAsUserRelativeLayout = (RelativeLayout) findViewById(R.id.rl_login_as_user);
        rlMain = (RelativeLayout) findViewById(R.id.activity_ibguser);

        loginButton.setOnClickListener(this);
        loginAsUserRelativeLayout.setOnClickListener(this);
    }


    //@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_as_ibguser:
                Utils.hide_keyboard(this);
                //InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    if (isFormValidated()) {
                        if (Utils.isNetworkAvailable(this)) {
                            login(emailEditText, passwordEditText);
                        } else {
                            Utils.showSnackBar(this, rlMain, context.getString(R.string.no_internet));
                        }
                    }

                break;
            case R.id.rl_login_as_user:
                loginAsUser();
                break;
        }
    }


    private boolean isFormValidated() {

        if (TextUtils.isEmpty(emailEditText.getText().toString())) {
            emailEditText.requestFocus();
            emailEditText.setError(getResources().getString(R.string.error_enter_email_id));
            return false;
        } else if (!Utils.isValidMail(emailEditText.getText().toString())) {
            emailEditText.requestFocus();
            emailEditText.setError(getResources().getString(R.string.error_invalid_email_id));
            return false;
        } else {
            emailEditText.setError(null);
        }


        if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordEditText.requestFocus();
            passwordEditText.setError(getResources().getString(R.string.error_enter_pwd));
            return false;
        } else {
            passwordEditText.setError(null);
        }
        return true;
    }

    private void loginAsUser() {
        intent = new Intent(LoginAsIbgUserActivity.this, LoginActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_right_in, R.anim.left_to_right_out);
        finish();
    }

    private void login(EditText emailEditText, EditText passwordEditText) {
        Utils.showProgressBar(this, progressBar);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);
        params.put(Constants.USER_EMAIL, emailEditText.getText().toString());
        params.put(Constants.USER_PWD, passwordEditText.getText().toString());
        params.put(Constants.DEVICE_ID, Utils.getAndroidId(this));
        params.put(Constants.REG_ID, "sdddasd");
        params.put(Constants.DEVICE_TYPE, Constants.ANDROID);

        CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_SIGN_IN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response==" + response);
                    if (response.get("response_code").equals("1")) {
                        Utils.hideProgressBar(LoginAsIbgUserActivity.this, progressBar);
                        // To end Google Session
                        //  response=={"response_code":"1","message":"Success","data":{"id":"23","role_id":"0","user_mobile":"1234567890","user_email":"Test@test.com","username":"Test","name":"","first_name":"Test","last_name":"","user_dob":"2016-10-17","user_gender":"","user_pic":"","user_address":"","user_city":"","user_state":"","user_country":"","country_code":"","user_profile_status":"","login_status":"offline","note":"","creation_date":"0000-00-00 00:00:00","modification_date":"0000-00-00 00:00:00","user_id":"23"}}
                        JSONObject jsonObject = response.getJSONObject("data");
                        Preferences.setStringPreference(context, USER_ID, jsonObject.optString("id"));
                        String roleId = jsonObject.optString("role_id");

                        Preferences.setBooleanPreference(LoginAsIbgUserActivity.this, Constants.LOGIN, true);
                        Preferences.setIntPreference(LoginAsIbgUserActivity.this,Constants.LOGIN_AS,2);
                        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        //Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                        Utils.hideProgressBar(LoginAsIbgUserActivity.this, progressBar);
                        Utils.showSnackBar(LoginAsIbgUserActivity.this, rlMain, response.getString("message"));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    //Toast.makeText(context, "Error in Login", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressBar(LoginAsIbgUserActivity.this, progressBar);
                    Utils.showSnackBar(LoginAsIbgUserActivity.this, rlMain, "Error in Login");
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                Utils.hideProgressBar(LoginAsIbgUserActivity.this, progressBar);
                Utils.showSnackBar(LoginAsIbgUserActivity.this, rlMain, error.toString());
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, Constants.URL_SIGN_IN);


    }

}
