package com.ibghub.search.listing.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.activity.ListingActivity;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.database.tables.CategoryTable;
import com.ibghub.search.listing.pojo.CategoriesPojo;
import com.ibghub.search.listing.pojo.ListingPojo;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Utils;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manoj on 1/11/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CategoriesPojo> categoriesList = new ArrayList<>();
    private ArrayList<CategoriesPojo> subCategoriesList = new ArrayList<>();
    private ArrayList<ListingPojo> listingArrayList = new ArrayList<>();
    RelativeLayout rlMain;
    ProgressBar progressBar;
    Activity activity;
    Animation animZoomOut;
    //final LayoutInflater layoutInflater;

    public CategoryAdapter(Context context, ArrayList<CategoriesPojo> categoriesList, RelativeLayout rlMain, ProgressBar progressBar) {
        this.context = context;
        this.categoriesList = categoriesList;
        this.rlMain = rlMain;
        this.progressBar = progressBar;
        this.activity = (Activity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_layout_categories, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CategoriesPojo category = categoriesList.get(position);
        holder.name.setText(category.getCategoryName());
        holder.catImage.setImageResource(category.getCategoryImage());

        holder.rlCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // animZoomOut = AnimationUtils.loadAnimation(context, R.anim.zoom_out);
                //holder.rlCategory.startAnimation(animZoomOut);

                if (Utils.isNetworkAvailable(context)) {
                    //listing(category.getCateId());
                    subCategoriesList = CategoryTable.getInstance().getSubCategories(category.getCateId());
                    if (subCategoriesList.size() > 0) {
                        showSubCatList(category.getCateId());
                    } else {
                        listing(category.getCateId(), null);
                        /*Intent intent = new Intent(context, ListingDetailActivity.class);
                        context.startActivity(intent);*/
                    }

                } else {
                    Utils.showSnackBar(context, rlMain, context.getString(R.string.no_internet));
                }
            }
        });
    }

    public void showSubCatList(final String catId) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        //builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle(Constants.SELECT_SUBCATEGORY);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1);
        for (CategoriesPojo subCat : subCategoriesList) {
            arrayAdapter.add(subCat.getCategoryName());
        }
/*        arrayAdapter.add("Hardik");
        arrayAdapter.add("Archit");
        arrayAdapter.add("Jignesh");
        arrayAdapter.add("Umang");
        arrayAdapter.add("Gatti");*/

        builderSingle.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CategoriesPojo subCat = subCategoriesList.get(which);
                        listing(catId, subCat.getCateId());
                        /*AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                context);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected Item is");
                        builderInner.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builderInner.show();*/
                    }
                });
        builderSingle.show();
    }

    @Override
    public int getItemCount() {
        Integer size = (categoriesList != null) ? categoriesList.size() : 0;
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView catImage;
        private TextView name;
        private RelativeLayout rlCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            catImage = (ImageView) itemView.findViewById(R.id.iv_category);
            name = (TextView) itemView.findViewById(R.id.tv_category_name);
            rlCategory = (RelativeLayout) itemView.findViewById(R.id.rl_cat);
        }
    }


    private void listing(String catId, String subCatId) {
        Utils.showProgressBar(activity, progressBar);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);
        params.put(Constants.CAT_ID, catId);
        if (subCatId != null) {
            params.put(Constants.SUB_CAT_ID, subCatId);
        }
        /*params.put(Constants.DEVICE_ID, Utils.getAndroidId(context));
        params.put(Constants.REG_ID, "sdddasd");
        params.put(Constants.DEVICE_TYPE, Constants.ANDROID);*/


        CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_LISTING, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response==" + response);
                    if (response.get("status").equals(true)) {
                        Utils.hideProgressBar(activity, progressBar);
                        // To end Google Session
                        //  response=={"response_code":"1","message":"Success","data":{"id":"23","role_id":"0","user_mobile":"1234567890","user_email":"Test@test.com","username":"Test","name":"","first_name":"Test","last_name":"","user_dob":"2016-10-17","user_gender":"","user_pic":"","user_address":"","user_city":"","user_state":"","user_country":"","country_code":"","user_profile_status":"","login_status":"offline","note":"","creation_date":"0000-00-00 00:00:00","modification_date":"0000-00-00 00:00:00","user_id":"23"}}
                        if (response.has("data")) {
                            listingArrayList.clear();
                            JSONArray data = response.getJSONArray("data");
                            /*for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = (JSONObject) data.get(i);
                                ListingPojo list = new Gson().fromJson(jsonObject.toString(),ListingPojo.class);
                                listingArrayList.add(list);
                            }*/

                            if (data.length() > 0) {
                                Intent intent = new Intent(activity, ListingActivity.class);
                                intent.putExtra("listing", data.toString());
                                activity.startActivity(intent);
                            } else {
                                Utils.showSnackBar(context, rlMain, "No Listing Available");
                            }
                        } else {
                            Utils.showSnackBar(context, rlMain, "No Data Available");
                        }

                    } else {
                        Utils.hideProgressBar(activity, progressBar);
                        Utils.showSnackBar(context, rlMain, response.getString("message"));
                        //Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    //Toast.makeText(context, "Error in Login", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressBar(activity, progressBar);
                    Utils.showSnackBar(context, rlMain, "Error in Login");
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show();
                Utils.hideProgressBar(activity, progressBar);
                Utils.showSnackBar(context, rlMain, error.toString());
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, Constants.URL_LISTING);

    }
}
