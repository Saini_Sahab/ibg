package com.ibghub.search.listing.database.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;


import com.ibghub.search.listing.database.AbstractEntityTable;
import com.ibghub.search.listing.database.DatabaseManager;


/**
 * Storage with User Details.
 *
 * @author Parmanand
 */

public class CountryTable extends AbstractEntityTable {


    private static final String NAME = "Country";
    private final static CountryTable instance;
    static {
        instance = new CountryTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }    private static final String[] PROJECTION = new String[]{Fields._ID,
            Fields.COUNTRY_NAME};
    private final DatabaseManager databaseManager;
    private final Object insertNewLoginLock;
    private SQLiteStatement insertNewLoginStatement;

    private CountryTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        insertNewLoginStatement = null;
        insertNewLoginLock = new Object();
    }

    public static CountryTable getInstance() {
        return instance;
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + NAME + " (" + Fields._ID
                + " INTEGER PRIMARY KEY,"
                + Fields.COUNTRY_NAME + " TEXT " +")";
        DatabaseManager.execSQL(db, sql);
        sql = "CREATE INDEX " + NAME + "_list ON " + NAME + " ("
                + Fields._ID
                + " ASC)";
        DatabaseManager.execSQL(db, sql);

    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        super.migrate(db, toVersion);
        String sql;
        switch (toVersion) {

            default:
                break;
        }
    }

    /**
     * Save new user to the database.
     *
     * @return Assigned id.
     */
    /*public long add(CountryPojo countryData) {


        String countryId = countryData.getId();
        String countryName = countryData.getCountry();

        synchronized (insertNewLoginLock) {
            if (insertNewLoginStatement == null) {
                SQLiteDatabase db = databaseManager.getWritableDatabase();
                insertNewLoginStatement = db.compileStatement("INSERT INTO "
                        + NAME + " (" + Fields._ID + ", " + Fields.COUNTRY_NAME + ") VALUES "
                        + "(?,?);");
            }
            insertNewLoginStatement.bindString(1, countryId);
            insertNewLoginStatement.bindString(2, countryName);

            return insertNewLoginStatement.executeInsert();
        }

    }*/

    //return true if there is any user present
    public boolean hasUser(String user) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        Cursor cr = db.query(NAME, null, Fields.COUNTRY_NAME + " = ? ", new String[]{user},
                null, null, null);

        return ((cr.getCount() > 0)) ? true : false;
    }
/*
    //Update the Other Details if its already in database
    public long updateEmail(String userName, String email, String name, String phoneNumber, String imageUrl, String status) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Fields.USER, userName);
        values.put(Fields.EMAIL_ID, email);
        values.put(Fields.NAME, name);
        values.put(Fields.PHONE_NUMBER, phoneNumber);
        values.put(Fields.PROFILE_IMAGE_PATH, imageUrl);
        values.put(Fields.STATUS, status);

        return db.update(NAME, values, Fields.USER + " = ?",
                new String[]{userName});
    }*/

    /*public ArrayList<CountryPojo> getCountryDetails() {

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ArrayList<CountryPojo> countryList   = new ArrayList<>();
        CountryPojo entity;
        Cursor cursor = db.query(NAME, PROJECTION, null, null, null, null, null);
        //System.out.println("cntycnt"+cursor.getCount());
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            entity = new CountryPojo(cursor.getString(cursor.getColumnIndex(Fields._ID)),cursor.getString(cursor.getColumnIndex(Fields.COUNTRY_NAME)) );
            countryList.add(entity);
            cursor.moveToNext();
        }
        cursor.close();
        return countryList;
    }*/

    private static final class Fields implements AbstractEntityTable.Fields {



        public static final String COUNTRY_NAME = "country";

        private Fields() {
        }


    }




}
