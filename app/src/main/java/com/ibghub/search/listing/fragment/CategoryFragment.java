package com.ibghub.search.listing.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.activity.CategoriesActivity;
import com.ibghub.search.listing.adapter.CategoryAdapter;
import com.ibghub.search.listing.decorator.DividerItemDecoration;
import com.ibghub.search.listing.pojo.CategoriesPojo;

import java.util.ArrayList;

public class CategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private RelativeLayout rlMain;
    private CategoryAdapter adapter;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private ArrayList<CategoriesPojo> categoriesList;
    private GridLayoutManager layoutManager;


    private String[] categoryNames = {"Plumber", "Dry Cleaners", "Placement", "Coaching & Instititutes", "Tour & Travels",
            "Hotels", "Doctors", "Repairing", "Electronics"};

    private int[] categoryImages = {R.drawable.plumber, R.drawable.drycleaners, R.drawable.placements, R.drawable.coaching,
            R.drawable.tourntravel, R.drawable.hotels, R.drawable.doctor, R.drawable.repairing, R.drawable.electronics};

   // private OnFragmentInteractionListener mListener;

    public CategoryFragment() {
        // Required empty public constructor
    }

    public static CategoryFragment newInstance() {
        CategoryFragment fragment = new CategoryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_category, container, false);
        settigToolBar();
        setHasOptionsMenu(true);
        bind(view);
        return view;
    }

    public void bind(View view) {
        rlMain = (RelativeLayout) getActivity().findViewById(R.id.activity_categories);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_categories);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        categoriesList = new ArrayList<>();
        createObjects();
        adapter = new CategoryAdapter(this.getActivity(), categoriesList, rlMain, progressBar);
        layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);

        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.bg);
        recyclerView.addItemDecoration(new DividerItemDecoration(horizontalDivider, horizontalDivider, 3));

        recyclerView.setAdapter(adapter);
    }

    private void settigToolBar(){
        CategoriesActivity.pager.setCurrentItem(1);
    }


    private void createObjects() {
        for (int i = 0; i < categoryNames.length; i++) {
            CategoriesPojo category = new CategoriesPojo(String.valueOf(i + 1), categoryNames[i], categoryImages[i]);
            categoriesList.add(category);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main,menu);
        //menu.add(0,1,0,"tit;e").setIcon(R.drawable.ic_search_white_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.search:
                CategoriesActivity.pager.setCurrentItem(2);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
