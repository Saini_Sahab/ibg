package com.ibghub.search.listing.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.pojo.CategoriesPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<CategoriesPojo> expandableListTitle;
    private ArrayList<CategoriesPojo> arraylist;
    private HashMap<String, ArrayList<CategoriesPojo>> expandableListDetail;
    private int[] color = {R.color.one, R.color.two, R.color.three};
    private Bitmap mIcon_val;
    private int[] catMarkers = {R.drawable.ic_marker_plumber,
            R.drawable.ic_marker_drycleaner,
            R.drawable.ic_marker_placement,
            R.drawable.ic_marker_coaching,
            R.drawable.ic_marker_tourntravel,
            R.drawable.ic_marker_hotels,
            R.drawable.ic_marker_doctor,
            R.drawable.ic_marker_repairing,
            R.drawable.ic_marker_electronics};

    public ExpandableListAdapter(Context context, ArrayList<CategoriesPojo> expandableListTitle,
                                 HashMap<String, ArrayList<CategoriesPojo>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        arraylist = new ArrayList<CategoriesPojo>();
        arraylist.addAll(expandableListTitle);
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition).getCategoryName())
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        //final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        CategoriesPojo pic = (CategoriesPojo) getChild(listPosition, expandedListPosition);
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        LinearLayout expandedListLy = (LinearLayout) convertView
                .findViewById(R.id.listitemLy);

        //ImageView expandedListImgView = (ImageView) convertView.findViewById(R.id.expandImgView);
        //expandedListImgView.setImageResource(catMarkers[listPosition]);
        expandedListTextView.setText(pic.getCategoryName());
        //String commentUserPhoto = pic.getUserpic();
       /* if (TextUtils.isEmpty(commentUserPhoto)) {
            Picasso.with(expandedListImgView.getContext()).load(R.drawable.noimage).placeholder(R.drawable.noimage).into(expandedListImgView);
        } else {
            Picasso.with(expandedListImgView.getContext()).load(commentUserPhoto).placeholder(R.drawable.noimage).into(expandedListImgView);
        }*/
        //Log.e("SUBCAT_PIC", pic.getUserpic());
        //expandedListLy.setBackgroundResource(color[listPosition % color.length]);

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition).getCategoryName())
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        CategoriesPojo categoryList = ((CategoriesPojo) getGroup(listPosition));
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        ImageView expandImageView = (ImageView) convertView.findViewById(R.id.iv_expand);
        ImageView expandParentImgView = (ImageView) convertView.findViewById(R.id.expandParentImgView);
        expandParentImgView.setImageResource(catMarkers[listPosition]);

//        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText("" + categoryList.getCategoryName());
       //listTitleTextView.setBackgroundResource(color[listPosition % color.length]);
        //String photo = categoryList.getCategoryPic();
        /*if (TextUtils.isEmpty(photo) || photo == null || photo.equalsIgnoreCase("null")) {
            Picasso.with(expandParentImgView.getContext()).load(R.drawable.noimage).placeholder(R.drawable.noimage).into(expandParentImgView);
        } else {
            Picasso.with(expandParentImgView.getContext()).load(photo).placeholder(R.drawable.noimage).into(expandParentImgView);
        }*/
        if (isExpanded) {
            expandImageView.setRotation(90);
        } else {
            expandImageView.setRotation(0);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase();

        expandableListTitle.clear();
        if (charText.length() == 0) {
            expandableListTitle.addAll(arraylist);

        } else {
            for (CategoriesPojo categoryList : arraylist) {
                if (charText.length() != 0 && categoryList.getCategoryName().toLowerCase().contains(charText)) {
                    expandableListTitle.add(categoryList);
                } else if (charText.length() != 0) {
                    for (CategoriesPojo subCatPicList : this.expandableListDetail.get(categoryList.getCategoryName())) {
                        if (subCatPicList.getCategoryName().toLowerCase().contains(charText)) {
                            if (expandableListTitle != null) {
                                expandableListTitle.remove(categoryList);
                            }
                            expandableListTitle.add(categoryList);
                        }
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

}