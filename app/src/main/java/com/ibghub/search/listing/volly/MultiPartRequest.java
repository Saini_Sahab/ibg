package com.ibghub.search.listing.volly;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.parse.entity.mime.HttpMultipartMode;
import com.parse.entity.mime.MultipartEntity;
import com.parse.entity.mime.content.FileBody;
import com.parse.entity.mime.content.StringBody;

import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class MultiPartRequest extends Request<JSONObject> {
    private MultipartEntity entity;
    private HttpEntity httpentity;
    private String FILE_PART_NAME;

    private final Response.Listener<JSONObject> mListener;
    private final File mFilePart;
    private final Map<String, String> mStringPart;

    public MultiPartRequest(String url, Response.ErrorListener errorListener, Response.Listener<JSONObject> listener, File file, Map<String, String> mStringPart, String fileParamName) {
        super(Method.POST, url, errorListener);
        entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        mListener = listener;
        mFilePart = file;
        this.mStringPart = mStringPart;
        FILE_PART_NAME = fileParamName;
        buildMultipartEntity();
    }

    public void addStringBody(String param, String value) {
        mStringPart.put(param, value);
    }

    private void buildMultipartEntity() {
        try {
            entity.addPart(FILE_PART_NAME, new FileBody(mFilePart));
            for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
                entity.addPart(entry.getKey(), new StringBody(entry.getValue()));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBodyContentType() {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity = entity;
            httpentity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        mListener.onResponse(response);
    }
}