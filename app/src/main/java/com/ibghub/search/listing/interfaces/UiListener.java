package com.ibghub.search.listing.interfaces;

/**
 * Created by Lenovo on 01-Jan-17.
 */

public interface UiListener {
    public void update(String type, Object data);
}
