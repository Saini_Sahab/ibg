package com.ibghub.search.listing.pojo;

/**
 * Created by Lenovo on 01-Sep-16.
 */
public class CountryPojo {

    private String name;

    public CountryPojo(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
