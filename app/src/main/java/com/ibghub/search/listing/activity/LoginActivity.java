package com.ibghub.search.listing.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Preferences;
import com.ibghub.search.listing.utility.Utils;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RECEIVE_SMS_PERMISSION_CODE = 9;
    private EditText nameEditText, phoneEditText;
    private Button loginButton;
    private Context context;
    private ProgressBar progressBar;
    private ImageView loginAsIbgIV;
    private RelativeLayout loginAsIbgRelativeLayout;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFullScreen(this);
        setContentView(R.layout.activity_login);
        context = this;
        bind();
    }


    private void bind() {
        nameEditText = (EditText) findViewById(R.id.et_name);
        phoneEditText = (EditText) findViewById(R.id.et_phone);
        loginButton = (Button) findViewById(R.id.login_as_user);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        loginAsIbgIV = (ImageView) findViewById(R.id.iv_login_as_ibg);
        loginAsIbgRelativeLayout = (RelativeLayout) findViewById(R.id.rl_login_as_ibg);

        loginButton.setOnClickListener(this);
        loginAsIbgIV.setOnClickListener(this);
        loginAsIbgRelativeLayout.setOnClickListener(this);
    }

    //@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_as_user:
                Utils.hide_keyboard(this);
                if (isFormValidated()) {
                    if (Utils.isPermissionGranted(LoginActivity.this,
                            Manifest.permission.RECEIVE_SMS)) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        String name = nameEditText.getText().toString().trim();
                        String phone = phoneEditText.getText().toString().trim();
                        login(name, phone);
                    }else {
                        Utils.requestPermissions(this, RECEIVE_SMS_PERMISSION_CODE,
                                Manifest.permission.RECEIVE_SMS);
                    }

                }
                break;
            case R.id.iv_login_as_ibg:
                loginAsIbgUser();
                break;
            case R.id.rl_login_as_ibg:
                loginAsIbgUser();
                break;
        }
    }



    private void loginAsIbgUser() {
        intent = new Intent(LoginActivity.this, LoginAsIbgUserActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        //finish();
        overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
        finish();
        //overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    private boolean isFormValidated() {
        if (TextUtils.isEmpty(nameEditText.getText().toString())) {
            nameEditText.setError(getResources().getString(R.string.error_enter_full_name));
            nameEditText.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(phoneEditText.getText().toString())) {
            phoneEditText.setError(getResources().getString(R.string.error_enter_phone));
            phoneEditText.requestFocus();
            return false;
        }

        if (phoneEditText.getText().toString().length()<10) {
            phoneEditText.setError(getResources().getString(R.string.error_enter_phone));
            phoneEditText.requestFocus();
            return false;
        }
        return true;
    }

    private void login(final String name, String phone) {
        Utils.showProgressBar(this, progressBar);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);
        params.put(Constants.USER_MOBILE,phone);
        /*params.put(Constants.USER_NAME, name);
        params.put(Constants.USER_MOBILE, phone);
        params.put(Constants.DEVICE_ID, Utils.getAndroidId(this));
        params.put(Constants.REG_ID, "sdddasd");
        params.put(Constants.DEVICE_TYPE, Constants.ANDROID);*/

        CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_OTP, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response==" + response);
                    if (response.get("response_code").equals("1")) {
                        Utils.hideProgressBar(LoginActivity.this, progressBar);
                        // To end Google Session
                        //  response=={"response_code":"1","message":"Success","data":{"id":"23","role_id":"0","user_mobile":"1234567890","user_email":"Test@test.com","username":"Test","name":"","first_name":"Test","last_name":"","user_dob":"2016-10-17","user_gender":"","user_pic":"","user_address":"","user_city":"","user_state":"","user_country":"","country_code":"","user_profile_status":"","login_status":"offline","note":"","creation_date":"0000-00-00 00:00:00","modification_date":"0000-00-00 00:00:00","user_id":"23"}}
                        /*JSONObject jsonObject = response.getJSONObject("data");
                        Preferences.setStringPreference(context, Constants.USER_ID, jsonObject.optString("id"));
                        Preferences.setStringPreference(context, Constants.USER_NAME, jsonObject.optString("name"));
                        Preferences.setStringPreference(context, Constants.USER_MOBILE, jsonObject.optString("user_mobile"));
                        String roleId = jsonObject.optString("role_id");*/

                        String otp = response.getString("otp");

                        Preferences.setStringPreference(LoginActivity.this,Constants.OTP,otp);
                        Preferences.setStringPreference(LoginActivity.this,Constants.USER_NAME,name);

                        Preferences.setBooleanPreference(LoginActivity.this, Constants.LOGIN, true);
                        Preferences.setIntPreference(LoginActivity.this,Constants.LOGIN_AS,1);
                        intent = new Intent(LoginActivity.this, OtpActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                        Utils.hideProgressBar(LoginActivity.this, progressBar);

                        /*Preferences.setBooleanPreference(LoginActivity.this, Constants.LOGIN, true);
                        Preferences.setIntPreference(LoginActivity.this,Constants.LOGIN_AS,1);
                        intent = new Intent(LoginActivity.this, CategoriesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();*/
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(context, "Error in Login", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressBar(LoginActivity.this, progressBar);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                Utils.hideProgressBar(LoginActivity.this, progressBar);
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, Constants.URL_USER_CREATE);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RECEIVE_SMS_PERMISSION_CODE:
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        String name = nameEditText.getText().toString().trim();
                        String phone = phoneEditText.getText().toString().trim();
                        login(name, phone);
                    } else {

                    }
                }
                break;
        }
    }
}
