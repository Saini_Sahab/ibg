package com.ibghub.search.listing.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.adapter.ListingAdapter;
import com.ibghub.search.listing.pojo.ListingPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class ListingActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    RecyclerView rvListing;
    ListingAdapter listingAdapter;
    Toolbar toolbar;

    SearchView.SearchAutoComplete mQueryTextView;

    LinearLayoutManager layoutManager;
    ArrayList<ListingPojo> listingArrayList = new ArrayList<>();

    //@RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        if (getIntent().hasExtra("listing")) {
            try {
                JSONArray jsonArray = new JSONArray(getIntent().getStringExtra("listing"));
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.getJSONObject(i);
                    String title = jsonObject.getString("title");
                    String phone = jsonObject.getString("phone");
                    String country = jsonObject.getString("country");
                    String zipcode = jsonObject.getString("zip_code");
                    String openTime = jsonObject.getString("open_time");
                    String closeTime = jsonObject.getString("close_time");
                    String description = jsonObject.getString("description");
                    String website = jsonObject.getString("website");
                    String latitude = jsonObject.getString("latitude");
                    String longitude = jsonObject.getString("longitude");

                    ListingPojo listingPojo = new ListingPojo();//new Gson().fromJson(jsonObject.toString(),ListingPojo.class);
                    listingPojo.setTitle(title);
                    listingPojo.setPhone(phone);
                    listingPojo.setCountry(country);
                    listingPojo.setZipCode(zipcode);
                    listingPojo.setOpenTime(openTime);
                    listingPojo.setCloseTime(closeTime);
                    listingPojo.setDescription(description);
                    listingPojo.setWebsite(website);
                    listingPojo.setLatitude(latitude);
                    listingPojo.setLongitude(longitude);

                    listingArrayList.add(listingPojo);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        settigToolBar();
        bind();
    }


    private void bind() {

        rvListing = (RecyclerView) findViewById(R.id.rv_listing);
        listingAdapter = new ListingAdapter(this, R.layout.adapter_layout_listing, listingArrayList);
        layoutManager = new LinearLayoutManager(this);
        rvListing.setLayoutManager(layoutManager);
        rvListing.setAdapter(listingAdapter);
    }

    private void settigToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Listing");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        setSearchTextColour(searchView);
        //setSearchIcons(searchView);
        searchView.setOnQueryTextListener(this);
        return true;
    }


    private void setSearchTextColour(SearchView searchView) {
        ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(Color.parseColor("#50ffffff"));
        ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setTextColor(Color.WHITE);
        ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHint("Search...");
        ((ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn)).setImageResource(R.drawable.ic_close_white_24dp);

        /*int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchPlate = (EditText) searchView.findViewById(searchPlateId);
        searchPlate.setTextColor(getResources().getColor(R.color.white));
       // searchPlate.setBackgroundResource(R.drawable.edit_text_holo_light);
        searchPlate.setImeOptions(EditorInfo.IME_ACTION_SEARCH);*/
    }


    private void setSearchIcons(SearchView searchView) {
        try {
            Field searchField = SearchView.class.getDeclaredField("mCloseButton");
            searchField.setAccessible(true);
            ImageView closeBtn = (ImageView) searchField.get(searchView);
            closeBtn.setImageResource(R.drawable.ic_close_white_24dp);

/*            searchField = SearchView.class.getDeclaredField("mVoiceButton");
            searchField.setAccessible(true);
            ImageView voiceBtn = (ImageView) searchField.get(searchView);
            voiceBtn.setImageResource(R.drawable.ic_menu_voice_input);*/

            ImageView searchButton = (ImageView) searchView.findViewById(R.id.search);
            searchButton.setImageResource(R.drawable.ic_search_white_24dp);

        } catch (NoSuchFieldException e) {
            Log.e("SearchView", e.getMessage(), e);
        } catch (IllegalAccessException e) {
            Log.e("SearchView", e.getMessage(), e);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listingAdapter.getFilter().filter(newText.toString());
        return true;
    }
}
