package com.ibghub.search.listing.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.activity.ListingDetailActivity;
import com.ibghub.search.listing.pojo.ListingPojo;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Manoj on 1/11/2016.
 */
public class ListingAdapter extends RecyclerView.Adapter<ListingAdapter.ViewHolder> implements Filterable{

    private Context context;
    private ArrayList<ListingPojo> myList = new ArrayList<>();
    ArrayList<ListingPojo> filteredMyList;
    private ItemFilter mFilter = new ItemFilter();

    //final LayoutInflater layoutInflater;

    public ListingAdapter(Context context, int resource, ArrayList<ListingPojo> myList) {
        this.context = context;
        this.myList = myList;
        this.filteredMyList = myList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_layout_listing, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        final ListingPojo list = filteredMyList.get(position);
        holder.tvName.setText(list.getTitle());
        holder.tvPhone.setText("Phone : " +list.getPhone());
        if(!list.getCountry().equalsIgnoreCase("") || !list.getZipCode().equalsIgnoreCase("")) {
            holder.tvAddress.setText((list.getCountry().equalsIgnoreCase("") ? "" : list.getCountry() )
                    + (list.getZipCode().equalsIgnoreCase("") ? "" : " Zipcode : " + list.getZipCode()));
        }

        if(!list.getOpenTime().equalsIgnoreCase("") || !list.getCloseTime().equalsIgnoreCase("")){
            String openTime = (list.getOpenTime().equalsIgnoreCase("")?"":"Open Time : " + list.getOpenTime() + " ");
            String closeTime = (list.getCloseTime().equalsIgnoreCase("")?"":"Close Time : " + list.getCloseTime());
            holder.tvTime.setText(openTime + closeTime);
        }
        if(position%2 == 0){
            holder.rlListing.setBackgroundColor(ContextCompat.getColor(context,R.color.light_gray));
        }else {
            holder.rlListing.setBackgroundColor(Color.WHITE);
        }

        final TextDrawable drawable = TextDrawable.builder()
                .buildRound((list.getTitle().charAt(0)+"").toUpperCase(), ContextCompat.getColor(context,R.color.colorPrimary));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.civImage.setBackground(drawable);
        }

        holder.rlListing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListingDetailActivity.class);
                intent.putExtra("list_detail",list);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredMyList.size()>0?filteredMyList.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView civImage;
        private TextView tvName,tvPhone,tvAddress,tvTime;
        private RelativeLayout rlListing;

        public ViewHolder(View itemView) {
            super(itemView);
            civImage = (CircleImageView) itemView.findViewById(R.id.civ_name_image);
            tvName = (TextView) itemView.findViewById(R.id.tv_listing_name);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_listing_phone);
            tvAddress = (TextView) itemView.findViewById(R.id.tv_listing_address);
            tvTime = (TextView) itemView.findViewById(R.id.tv_listing_timing);
            rlListing = (RelativeLayout)itemView.findViewById(R.id.rl_listing);
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final ArrayList<ListingPojo> list = myList;
            int count = list.size();
            final ArrayList<ListingPojo> nlist = new ArrayList<ListingPojo>(count);
            //String filterableString ;
            for (int i = 0; i < count; i++) {
                if (list.get(i).getTitle().toLowerCase().contains(filterString)
                        ||list.get(i).getPhone().toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredMyList = (ArrayList<ListingPojo>) results.values;
            notifyDataSetChanged();
        }

    }

}
