package com.ibghub.search.listing.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.ibghub.search.listing.database.AbstractEntityTable;
import com.ibghub.search.listing.database.DatabaseManager;
import com.ibghub.search.listing.pojo.CategoriesPojo;
import com.ibghub.search.listing.pojo.CountryPojo;
import com.ibghub.search.listing.utility.Constants;

import java.util.ArrayList;

/**
 * Created by Lenovo on 06-Nov-16.
 */

public class CategoryTable extends AbstractEntityTable {


    private static final String NAME = "Category";
    private final static CategoryTable instance;

    static {
        instance = new CategoryTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    private static final String[] PROJECTION = new String[]{
            Fields.CATEGORY_ID,
            Fields.CATEGORY_NAME,
            Fields.PARENT_ID,
            Fields.STATUS
    };
    private final DatabaseManager databaseManager;
    private final Object insertNewLoginLock;
    private SQLiteStatement insertNewLoginStatement;

    private CategoryTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        insertNewLoginStatement = null;
        insertNewLoginLock = new Object();
    }

    public static CategoryTable getInstance() {
        return instance;
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + NAME + " ("
                + Fields.CATEGORY_ID + " TEXT ,"
                + Fields.CATEGORY_NAME + " TEXT ,"
                + Fields.PARENT_ID + " TEXT ,"
                + Fields.STATUS + " TEXT "
                + ")";
        DatabaseManager.execSQL(db, sql);
        sql = "CREATE INDEX " + NAME + "_list ON " + NAME + " ("
                + Fields.CATEGORY_ID
                + " ASC)";
        DatabaseManager.execSQL(db, sql);

    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        super.migrate(db, toVersion);
        String sql;
        switch (toVersion) {

            default:
                break;
        }
    }

    /**
     * Save new user to the database.
     *
     * @return Assigned id.
     */
    public long add(CategoriesPojo categoryData) {


        String catId = categoryData.getCateId();
        String catName = categoryData.getCategoryName();
        String parentId = categoryData.getParentId();
        String status = categoryData.getStatus();

        if (!hasCategory(catId)) {
            synchronized (insertNewLoginLock) {
                if (insertNewLoginStatement == null) {
                    SQLiteDatabase db = databaseManager.getWritableDatabase();
                    insertNewLoginStatement = db.compileStatement("INSERT INTO "
                            + NAME + " ("
                            + Fields.CATEGORY_ID + ", "
                            + Fields.CATEGORY_NAME + ", "
                            + Fields.PARENT_ID + ", "
                            + Fields.STATUS
                            + ") VALUES "
                            + "(?,?,?,?);");
                }

                if (catId == null) {
                    insertNewLoginStatement.bindNull(1);
                } else {
                    insertNewLoginStatement.bindString(1, catId);
                }

                if (catName == null) {
                    insertNewLoginStatement.bindNull(2);
                } else {
                    insertNewLoginStatement.bindString(2, catName);
                }

                if (parentId == null) {
                    insertNewLoginStatement.bindNull(3);
                } else {
                    insertNewLoginStatement.bindString(3, parentId);
                }

                if (status == null) {
                    insertNewLoginStatement.bindNull(4);
                } else {
                    insertNewLoginStatement.bindString(4, status);
                }

                return insertNewLoginStatement.executeInsert();
            }
        } else {
            return updateCategory(catId, catName, parentId, status);
        }
    }

    //Update the Other Details if its already in database
    public long updateCategory(String catId, String catName, String parentId, String status) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Fields.CATEGORY_ID, catId);
        values.put(Fields.CATEGORY_NAME, catName);
        values.put(Fields.PARENT_ID, parentId);
        values.put(Fields.STATUS, status);


        return db.update(NAME, values, Fields.CATEGORY_ID + " = ?",
                new String[]{catId});
    }

    //return true if there is any category
    public boolean hasCategory(String catId) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        Cursor cr = db.query(NAME, null, Fields.CATEGORY_ID + " = ? ", new String[]{catId},
                null, null, null);

        if (cr.getCount() > 0) {
            cr.close();
            return true;
        } else {
            cr.close();
            return false;
        }
    }

    private static final class Fields implements AbstractEntityTable.Fields {


        public static final String CATEGORY_ID = "cat_id";
        public static final String CATEGORY_NAME = "cat_name";
        public static final String PARENT_ID = "parent_id";
        public static final String STATUS = "status";

        private Fields() {
        }
    }


    public ArrayList<CategoriesPojo> getMainCategories() {

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ArrayList<CategoriesPojo> categoryList = new ArrayList<>();

        CategoriesPojo entity; /*= new CategoriesPojo("0",Constants.SELECT_CATEGORY,"0","0");
        categoryList.add(entity);*/
        //Cursor cursor = db.query(NAME, PROJECTION, null, null, null, null, null);
        Cursor cursor = db.query(NAME, null, Fields.PARENT_ID + " = ? ", new String[]{"0"},
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            entity = new CategoriesPojo(cursor.getString(cursor.getColumnIndex(Fields.CATEGORY_ID)),
                    cursor.getString(cursor.getColumnIndex(Fields.CATEGORY_NAME)),
                    cursor.getString(cursor.getColumnIndex(Fields.PARENT_ID)),
                    cursor.getString(cursor.getColumnIndex(Fields.STATUS)));
            categoryList.add(entity);
            cursor.moveToNext();
        }
        cursor.close();
        return categoryList;
    }

    public ArrayList<CategoriesPojo> getSubCategories(String parent_id) {

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ArrayList<CategoriesPojo> subCategoryList = new ArrayList<>();
        CategoriesPojo entity; /*= new CategoriesPojo("0",Constants.SELECT_SUBCATEGORY,"0","0");
        subCategoryList.add(entity);*/

        //Cursor cursor = db.query(NAME, PROJECTION, null, null, null, null, null);
        Cursor cursor = db.query(NAME, null, Fields.PARENT_ID + " = ? ", new String[]{parent_id},
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            entity = new CategoriesPojo(cursor.getString(cursor.getColumnIndex(Fields.CATEGORY_ID)),
                    cursor.getString(cursor.getColumnIndex(Fields.CATEGORY_NAME)),
                    cursor.getString(cursor.getColumnIndex(Fields.PARENT_ID)),
                    cursor.getString(cursor.getColumnIndex(Fields.STATUS)));
            subCategoryList.add(entity);
            cursor.moveToNext();
        }
        cursor.close();
        return subCategoryList;
    }
}
