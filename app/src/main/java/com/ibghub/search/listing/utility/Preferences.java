package com.ibghub.search.listing.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {
	public static final String PREF_NAME = Preferences.class.getName();
	private static SharedPreferences preferences;

	public static void setStringPreference(Context context, String KEY, String value) {
		preferences = context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putString(KEY, "" + value);
		editor.commit();
	}

	public static String getStringPreference(Context context, String KEY) {
		preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		return preferences.getString(KEY, "");
	}

	public static void setIntPreference(Context context, String KEY, int value) {
		preferences = context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putInt(KEY, value);
		editor.commit();
	}

	public static int getIntPreference(Context context, String KEY) {
		preferences = context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);

		return preferences.getInt(KEY, 0);
	}

	public static void setBooleanPreference(Context context, String KEY, boolean value) {
		preferences = context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean(KEY, value);
		editor.commit();
	}

	public static boolean getBooleanPreference(Context context, String KEY) {
		preferences = context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);

		return preferences.getBoolean(KEY, false);
	}

}