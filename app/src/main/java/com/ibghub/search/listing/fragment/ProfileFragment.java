package com.ibghub.search.listing.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.activity.AboutActivity;
import com.ibghub.search.listing.activity.ContactActivity;
import com.ibghub.search.listing.activity.FaqActivity;
import com.ibghub.search.listing.activity.FeedbackActivity;
import com.ibghub.search.listing.activity.HomeActivity;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match

    //private OnFragmentInteractionListener mListener;

    Animation animZoomin;
    ImageView bg;
    ImageView  btnHome,btnAbout, btnFaq, btnFeedback, btnContact;
    LinearLayout llHome, llAbout, llFaq, llFeedback, llContact;
    TextView tvHome, tvAbout, tvFaq, tvFeedback, tvContact;
    Intent intent;

    public ProfileFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        bind(view);
        return view;
    }

    private void bind(View view){
        bg = (ImageView)view.findViewById(R.id.bg);
        btnHome = (ImageView)view.findViewById(R.id.btn_home);
        btnAbout = (ImageView)view.findViewById(R.id.btn_about);
        btnFaq = (ImageView)view.findViewById(R.id.btn_faq);
        btnFeedback = (ImageView)view.findViewById(R.id.btn_feedback);
        btnContact = (ImageView)view.findViewById(R.id.btn_contact);
        llHome = (LinearLayout) view.findViewById(R.id.ll_home);
        llAbout = (LinearLayout)view.findViewById(R.id.ll_about);
        llFaq = (LinearLayout)view.findViewById(R.id.ll_faq);
        llFeedback = (LinearLayout)view.findViewById(R.id.ll_feedback);
        llContact = (LinearLayout)view.findViewById(R.id.ll_contact);
        tvHome = (TextView) view.findViewById(R.id.tv_home);
        tvAbout = (TextView)view.findViewById(R.id.tv_about);
        tvFaq = (TextView)view.findViewById(R.id.tv_faq);
        tvFeedback = (TextView)view.findViewById(R.id.tv_feedback);
        tvContact = (TextView)view.findViewById(R.id.tv_contact);

        llHome.setOnClickListener(this);
        llAbout.setOnClickListener(this);
        llContact.setOnClickListener(this);
        llFaq.setOnClickListener(this);
        llFeedback.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_home:
                unSelected();
                selected(llHome,tvHome,btnHome,R.drawable.ic_home_selected);
                intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.ll_about:
                unSelected();
                selected(llAbout,tvAbout,btnAbout,R.drawable.ic_about_selected);
                intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                break;

            case R.id.ll_feedback:
                unSelected();
                selected(llFeedback,tvFeedback,btnFeedback,R.drawable.ic_feedback_selected);
                intent = new Intent(getActivity(), FeedbackActivity.class);
                startActivity(intent);
                break;

            case R.id.ll_faq:
                unSelected();
                selected(llFaq,tvFaq,btnFaq,R.drawable.ic_faq_selected);
                intent = new Intent(getActivity(), FaqActivity.class);
                startActivity(intent);
                break;

            case R.id.ll_contact:
                unSelected();
                selected(llContact,tvContact,btnContact,R.drawable.ic_contact_selected);
                intent = new Intent(getActivity(), ContactActivity.class);
                startActivity(intent);
                break;
        }
    }


    private void unSelected() {
        btnHome.setImageResource(R.drawable.ic_home);
        btnAbout.setImageResource(R.drawable.ic_about);
        btnFaq.setImageResource(R.drawable.ic_faq);
        btnFeedback.setImageResource(R.drawable.ic_feedback);
        btnContact.setImageResource(R.drawable.ic_contact);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvHome.setTextColor(getResources().getColor(R.color.white, null));
            tvAbout.setTextColor(getResources().getColor(R.color.white, null));
            tvFaq.setTextColor(getResources().getColor(R.color.white, null));
            tvFeedback.setTextColor(getResources().getColor(R.color.white, null));
            tvContact.setTextColor(getResources().getColor(R.color.white, null));
        }else {
            tvHome.setTextColor(getResources().getColor(R.color.white));
            tvAbout.setTextColor(getResources().getColor(R.color.white));
            tvFaq.setTextColor(getResources().getColor(R.color.white));
            tvFeedback.setTextColor(getResources().getColor(R.color.white));
            tvContact.setTextColor(getResources().getColor(R.color.white));
        }
    }


    private void selected(LinearLayout linearLayout, TextView textView,final ImageView view, int icon){
        view.setImageResource(icon);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(getResources().getColor(R.color.colorAccent,null));
        }else {
            textView.setTextColor(getResources().getColor(R.color.colorAccent));
        }
        //animZoomin = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
        //linearLayout.startAnimation(animZoomin);


    }

}
