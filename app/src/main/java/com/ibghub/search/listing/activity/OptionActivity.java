package com.ibghub.search.listing.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.utility.Utils;

public class OptionActivity extends AppCompatActivity {

    Button btnRegister,btnCat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFullScreen(this);
        setContentView(R.layout.activity_option);

        btnRegister = (Button)findViewById(R.id.btn_register);
        btnCat = (Button)findViewById(R.id.btn_category);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OptionActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OptionActivity.this,CategoriesActivity.class);
                startActivity(intent);
            }
        });
    }
}
