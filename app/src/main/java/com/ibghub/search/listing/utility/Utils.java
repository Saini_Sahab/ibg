package com.ibghub.search.listing.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.ibghub.search.listing.pojo.ListingPojo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class Utils {

    static ProgressDialog pDialog;

    public static String getDateFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd");
            String newDateStr = postFormater.format(date).toUpperCase();
            System.out.println("Date  : " + newDateStr);
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getMonthFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("MMM");
            String newDateStr = postFormater.format(date).toUpperCase();
            System.out.println("Date  : " + newDateStr);
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getYearFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("yyyy");
            String newDateStr = postFormater.format(date).toUpperCase();
            System.out.println("Date  : " + newDateStr);
            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long getDateTimeMillisFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            long millis = Calendar.getInstance().getTime().getTime() - date.getTime();
            System.out.println("Date  : " + millis);
//            System.out.println("Date  : " + new Date(millis).toString());
            return millis;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

   /* public static void generateSwatches(final Context context, Bitmap bitmap, final CollapsingToolbarLayout collapsingToolbarLayout) {
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                int defaultColor = context.getResources().getColor(R.color.colorPrimary);
                collapsingToolbarLayout.setContentScrimColor(palette.getDarkMutedColor(defaultColor));
            }
        });
    }*/

    public static String generateCacheKeyWithParam(String url, Map<String, String> params) {
        for (Map.Entry<String, String> entry : params.entrySet()) {
            url += entry.getKey() + "=" + entry.getValue();
        }
        return url;
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getImagePath(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return path;
    }


    public static void hide_keyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        // Find the currently focused view, so we can grab the correct window
        // token from it.
        View view = activity.getCurrentFocus();
        // If no view currently has focus, create a new one, just so we can grab
        // a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Check Internet connection available or not.
     *
     * @param context : current activity context
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    /**
     * Open Dialog and Settings Internet connection available or not.
     *
     * @return boolean
     */

 /*   public static void displayAlert(final Context mContext) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle("Network Error");
        alertDialog.setMessage("Would you like to turn on data!");
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.fail);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }*/
    @SuppressWarnings("deprecation")
/*    public static void showAlertDialog(final Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.drawable.fail);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
            }
        });
        alertDialog.show();
    }*/

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Context context) {
        if (((Activity) context).getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public static void showSoftKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    public static PointF PointOnCircle(float radius, float angleInDegrees, PointF origin) {
        // Convert from degrees to radians via multiplication by PI/180
        float x = (float) (radius * Math.cos(angleInDegrees * Math.PI / 180F)) + origin.x;
        float y = (float) (radius * Math.sin(angleInDegrees * Math.PI / 180F)) + origin.y;
        return new PointF(x, y);
    }


    /**
     * Show Alert dialog when logout
     *
     * @param context Activity context.
     */

    public static void showLogoutConfirmDialog(final Context context) {
        new AlertDialog.Builder(context).setMessage("Are you sure to logout?").setTitle("Confirmation").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }

    public static void refreshGallery(Context context, File imageFile) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(imageFile); // out is your output
            // file
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);
        } else {
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    public static boolean isValidMail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String mobile) {
        return Patterns.PHONE.matcher(mobile).matches();
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static long setTime(String time) {
        System.out.println("time => " + time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        try {
            Date mDate = sdf.parse(time);
            calendar.setTime(mDate);
            System.out.println("Calender - Time in milliseconds : " + calendar.getTimeInMillis());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis();
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static String getAndroidId(Context mContext) {
        String androidId = null;
        try {
            androidId = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return androidId;
    }


    public static void showProgressBar(Activity activity, ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.requestFocus();
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public static void hideProgressBar(Activity activity, ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void showSnackBar(Context context, View v, String message) {
        Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_LONG);
        /*View snackbarLayout = snackbar.getView();
        TextView textView = (TextView)snackbarLayout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error_black_24dp, 0, 0, 0);
        textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelOffset(R.dimen.five_dp));*/
        snackbar.show();

    }

    public static void setFullScreen(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    //@TargetApi(Build.VERSION_CODES.M)
    public static boolean isPermissionGranted(Context context, String... perm) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissions = new ArrayList<>();
            for (String s : perm) {
                if (context.checkSelfPermission(s) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void requestPermissions(Context context, int code, String... perm) {
        Activity activity = (Activity) context;
        ArrayList<String> permissions = new ArrayList<>();
        for (String s : perm) {
            permissions.add(s);
        }
        activity.requestPermissions(permissions.toArray(new String[permissions.size()]),
                code);
    }

    public static String getAddressForMap(ListingPojo listing) {
        String address = "";

        if (listing.getCountry() != null) {
            if (!listing.getCountry().equals("")) {
                address = listing.getCountry();
            }
        }

        if(listing.getState() != null){
            if(!listing.getState().equals("")){
                if(address.equals("")){
                    address = listing.getState();
                }else {
                    address += " ," + listing.getState();
                }
            }
        }

        if(listing.getCity() != null){
            if(!listing.getCity().equals("")){
                if(address.equals("")){
                    address = listing.getCity();
                }else {
                    address += " ," + listing.getCity();
                }
            }
        }

        if (listing.getZipCode() != null) {
            if(!listing.getZipCode().equals("")) {
                if (address.equalsIgnoreCase("")) {
                    address = listing.getZipCode();
                } else {
                    address += " ," + listing.getZipCode();
                }
            }
        }

        return address;
    }

}