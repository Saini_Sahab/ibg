package com.ibghub.search.listing.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Preferences;
import com.ibghub.search.listing.utility.Utils;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.ibghub.search.listing.utility.Constants.BASE_URL;
import static com.ibghub.search.listing.utility.Constants.USER_ID;
import static com.ibghub.search.listing.utility.Constants.VOLLY_AUTH_KEY;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    CoordinatorLayout coordinatorLayout;
    RelativeLayout rlOtpMain;
    EditText etOtp1, etOtp2, etOtp3, etOtp4, etOtp5;
    Button btnOtp;
    private Context context;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        context = this;
        bind();
    }

    private void bind() {
        etOtp1 = (EditText) findViewById(R.id.et_num1);
        etOtp2 = (EditText) findViewById(R.id.et_num2);
        etOtp3 = (EditText) findViewById(R.id.et_num3);
        etOtp4 = (EditText) findViewById(R.id.et_num4);
        etOtp5 = (EditText) findViewById(R.id.et_num5);
        btnOtp = (Button) findViewById(R.id.btn_confirm_otp);
        rlOtpMain = (RelativeLayout) findViewById(R.id.rl_otp_main);
        //coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        registerReceiver(receiver, new IntentFilter("OTP"));
        btnOtp.setOnClickListener(this);
        etOtp1.addTextChangedListener(this);
        etOtp2.addTextChangedListener(this);
        etOtp3.addTextChangedListener(this);
        etOtp4.addTextChangedListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm_otp:
                checkOtp();
                break;
        }
    }

    private void checkOtp() {
        if (isValidate()) {
           // if (Utils.isNetworkAvailable(context)) {
                String otp = etOtp1.getText().toString() +
                        etOtp2.getText().toString() +
                        etOtp3.getText().toString() +
                        etOtp4.getText().toString();
                        //etOtp5.getText().toString();
                if(otp.equals(Preferences.getStringPreference(OtpActivity.this, Constants.OTP))){
                    Preferences.setStringPreference(OtpActivity.this,Constants.OTP,"");
                    Intent intent = new Intent(OtpActivity.this, CategoriesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else {
                    Utils.showSnackBar(OtpActivity.this,rlOtpMain,"OTP is not correct, Please try again");
                }
                //otpVerify("9898989898", otp);
           /* } else {
                showSnackBar();
            }*/
        }
    }

    private void showSnackBar() {
        final boolean[] runAgain = new boolean[1];
        runAgain[0] = false;
        Snackbar snackbar = Snackbar
                .make(rlOtpMain, "No internet connection!", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkOtp();
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    private boolean isValidate() {
        if (!TextUtils.isEmpty(etOtp1.getText())
                && !TextUtils.isEmpty(etOtp2.getText())
                && !TextUtils.isEmpty(etOtp3.getText())
                && !TextUtils.isEmpty(etOtp4.getText())) {
            return true;
        }

        return false;
    }

    private void otpVerify(String phone, String otp) {
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<String, String>();
        params.put("key", VOLLY_AUTH_KEY);
        params.put("user_mobile", phone);
        params.put("user_otp", otp);
        params.put("device_id", "112222");
        params.put("reg_id", "sdddasd");
        params.put("device_type", "android");

        CustomRequest request = new CustomRequest(Request.Method.POST, BASE_URL + "otp", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response==" + response);
                    if (response.get("response_code").equals("1")) {
                        progressBar.setVisibility(View.GONE);
                        // To end Google Session
                        //  response=={"response_code":"1","message":"Success","data":{"id":"23","role_id":"0","user_mobile":"1234567890","user_email":"Test@test.com","username":"Test","name":"","first_name":"Test","last_name":"","user_dob":"2016-10-17","user_gender":"","user_pic":"","user_address":"","user_city":"","user_state":"","user_country":"","country_code":"","user_profile_status":"","login_status":"offline","note":"","creation_date":"0000-00-00 00:00:00","modification_date":"0000-00-00 00:00:00","user_id":"23"}}
                        JSONObject jsonObject = response.getJSONObject("data");
                        Preferences.setStringPreference(context, USER_ID, jsonObject.optString("id"));
                        String roleId = jsonObject.optString("role_id");

                   /*    // Intent intent = new Intent(getApplicationContext(), Home2Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();*/
                    } else {
                        Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(context, "Error in Login", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, "signin");


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(etOtp1.hasFocus() && etOtp1.getText().length()>0){
            etOtp1.clearFocus();
            etOtp2.requestFocus();
            etOtp2.setCursorVisible(true);
        }else if(etOtp2.hasFocus() && etOtp2.getText().length()>0){
            etOtp2.clearFocus();
            etOtp3.requestFocus();
            etOtp3.setCursorVisible(true);
        }else if(etOtp3.hasFocus() && etOtp3.getText().length()>0){
            etOtp3.clearFocus();
            etOtp4.requestFocus();
            etOtp4.setCursorVisible(true);
        }/*else if(etOtp4.hasFocus()&& etOtp4.getText().length()>0){
            etOtp4.clearFocus();
            etOtp5.requestFocus();
            etOtp5.setCursorVisible(true);
        }*/
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String otp = intent.getStringExtra("OTP");
            String sms = intent.getStringExtra("SMS");
            if (!TextUtils.isEmpty(otp) && !TextUtils.isEmpty(sms)) {
                etOtp1.setText(otp.substring(0, 1));
                etOtp2.setText(otp.substring(1, 2));
                etOtp3.setText(otp.substring(2, 3));
                etOtp4.setText(otp.substring(3));
                checkOtp();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
