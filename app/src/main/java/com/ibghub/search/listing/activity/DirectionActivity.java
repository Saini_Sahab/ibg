package com.ibghub.search.listing.activity;

import android.Manifest;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.pojo.ListingPojo;
import com.ibghub.search.listing.utility.Utils;

public class DirectionActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final int GPS_PERMISSION_CODE = 5;
    GoogleMap mMap;
    Toolbar toolbar;
    ListingPojo listing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction);

        if (getIntent().hasExtra("list_detail")) {
            listing = (ListingPojo) getIntent().getSerializableExtra("list_detail");
        }

        settigToolBar();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void settigToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(listing.getTitle());
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            this.mMap = googleMap;

            if (Utils.isPermissionGranted(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                setSearchedCatMarker(listing);
                //setMyLocation(latLng);
            } else {
                Utils.requestPermissions(this, GPS_PERMISSION_CODE,
                        Manifest.permission.ACCESS_FINE_LOCATION);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setSearchedCatMarker(ListingPojo list) {
        String catAddress = Utils.getAddressForMap(list);
        LatLng latLng = new LatLng(Double.parseDouble(list.getLatitude()), Double.parseDouble(list.getLongitude()));
        String title = list.getTitle();
        String phone = list.getPhone();

        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();


        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title + ", " + phone)
                .snippet(catAddress));
               // .icon(BitmapDescriptorFactory.fromResource(catMarkers[Integer.parseInt(list.getCatId().toString()) - 1])));
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.map_marker_info_layout, null);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());


                return infoWindow;
            }
        });

    }
}

