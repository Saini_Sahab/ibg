package com.ibghub.search.listing.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.database.tables.CategoryTable;
import com.ibghub.search.listing.gps.GPSLocation;
import com.ibghub.search.listing.gps.GPSTracker;
import com.ibghub.search.listing.pojo.CategoriesPojo;
import com.ibghub.search.listing.pojo.CountryPojo;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Preferences;
import com.ibghub.search.listing.utility.Utils;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnTouchListener, AdapterView.OnItemSelectedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final String TAG = "RegisterActivity";
    private static final int GPS_PERMISSION_CODE = 5;

    TextInputLayout tilName, tilPhone, tilEmail, tilDob, tilPwd, tilConfPwd, tilDescription, tilWebsite, tilZipcode;
    EditText etName, etPhone, etEmail, etQuery, etDob, etPwd, etConfPwd, etDescription, etWebsite, etZipcode;
    Spinner spCountry, spCategory, spSubcategory;
    Button btnRegister;
    private ProgressBar progressBar;
    private Calendar myCalendar;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale, rbGender;
    RelativeLayout rlMain;
    Toolbar toolbar;

    ArrayList<CountryPojo> countryList;
    ArrayList<String> countries = new ArrayList<>();
    ArrayList<CategoriesPojo> categoriesList = new ArrayList<>();
    ArrayList<CategoriesPojo> subCategoriesList = new ArrayList<>();
    ArrayList<String> catArray = new ArrayList<>();
    ArrayList<String> subCatArray = new ArrayList<>();
    String catId, subCatId;

    private Context context;
    GPSTracker gps;
    private DatePickerDialog.OnDateSetListener date;
    double latitude, longitude;
    boolean catRequestCheck = true;
    GPSLocation gpsLocation;

    @Override
    protected void onStart() {
        super.onStart();
        if(gpsLocation == null) {
            gpsLocation = new GPSLocation(RegisterActivity.this);
        }
        gpsLocation.onStart();
    }

    //@TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = this;
        settigToolBar();
        bind();
        setCountryAdapter();
        setSubcategoryAdapter();
        setCategoryAdapter();
        removeError();
        getCategories();

        if (Utils.isPermissionGranted(RegisterActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            getLocation();
            if(gpsLocation == null){
                gpsLocation = new GPSLocation(RegisterActivity.this);
            }
            gpsLocation.init();
        } else {
            Utils.requestPermissions(RegisterActivity.this, GPS_PERMISSION_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
        }
    }

    private void bind() {
        rlMain = (RelativeLayout) findViewById(R.id.activity_register);
        tilName = (TextInputLayout) findViewById(R.id.til_name);
        tilPhone = (TextInputLayout) findViewById(R.id.til_phone);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);
        tilDob = (TextInputLayout) findViewById(R.id.til_dob);
        tilPwd = (TextInputLayout) findViewById(R.id.til_pwd);
        tilConfPwd = (TextInputLayout) findViewById(R.id.til_conf_pwd);
        tilDescription = (TextInputLayout) findViewById(R.id.til_description);
        tilWebsite = (TextInputLayout) findViewById(R.id.til_website);
        tilZipcode = (TextInputLayout) findViewById(R.id.til_zipcode);
        spCountry = (Spinner) findViewById(R.id.sp_country);
        spCategory = (Spinner) findViewById(R.id.sp_category);
        spSubcategory = (Spinner) findViewById(R.id.sp_sub_category);
        etName = (EditText) findViewById(R.id.et_Name);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPhone = (EditText) findViewById(R.id.et_phone);
        etQuery = (EditText) findViewById(R.id.et_description);
        etDob = (EditText) findViewById(R.id.et_dob);
        etPwd = (EditText) findViewById(R.id.et_pwd);
        etConfPwd = (EditText) findViewById(R.id.et_conf_pwd);
        etDescription = (EditText) findViewById(R.id.et_description);
        etWebsite = (EditText) findViewById(R.id.et_website);
        etZipcode = (EditText) findViewById(R.id.et_zipcode);

        rgGender = (RadioGroup) findViewById(R.id.rg_gender);
        rbMale = (RadioButton) findViewById(R.id.rb_male);
        rbFemale = (RadioButton) findViewById(R.id.rb_female);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        btnRegister = (Button) findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(this);
        tilDob.setOnClickListener(this);
        etDob.setOnClickListener(this);
        spCategory.setOnTouchListener(this);
        spCategory.setOnItemSelectedListener(this);
        spSubcategory.setOnItemSelectedListener(this);

        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateBirthDate();
            }
        };
    }

    private void settigToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Register");
        toolbar.setTitleTextColor(Color.WHITE);
        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isPermissionGranted(RegisterActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            //getLocation();
            gpsLocation.onResume();
            latitude = gpsLocation.getLatitude();
            longitude = gpsLocation.getLognitude();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gpsLocation.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gpsLocation.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void getLocation() {
       /* if (gps == null) {
            gps = new GPSTracker(this);
        }else{
            gps.getLocation();
        }

        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            Preferences.setStringPreference(this, Constants.LAT_LAST, String.valueOf(latitude));
            Preferences.setStringPreference(this, Constants.LONG_LAST, String.valueOf(longitude));
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }*/
    }

    private void updateBirthDate() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        //globalDate = sdf.format(myCalendar.getTime());
        etDob.setText(setDay(sdf.format(myCalendar.getTime())));
    }

    public void setCountryAdapter() {
        countryList = settingCountry();

        for (int i = 0; i < countryList.size(); i++) {
            if (i == 0) {
                countries.add(Constants.SELECT_COUNTRY);
            }
            countries.add(countryList.get(i).getName());
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spCountry.setAdapter(dataAdapter);
    }


    public void setCategoryAdapter() {
        categoriesList.clear();
        categoriesList = CategoryTable.getInstance().getMainCategories();

        if (categoriesList.size() == 0) {
            catArray.clear();
            catArray.add(Constants.SELECT_CATEGORY);
        } else {
            catArray.clear();
            catArray.add(Constants.SELECT_CATEGORY);
            for (CategoriesPojo categoriesPojo : categoriesList) {
                catArray.add(categoriesPojo.getCategoryName());
            }
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, catArray);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spCategory.setAdapter(dataAdapter);
    }

    public void setSubcategoryAdapter() {

        if (subCategoriesList.size() == 0) {
            spSubcategory.setVisibility(View.GONE);
        } else {
            spSubcategory.setVisibility(View.VISIBLE);
            subCatArray.clear();
            subCatArray.add(Constants.SELECT_SUBCATEGORY);
            for (CategoriesPojo categoriesPojo : subCategoriesList) {
                subCatArray.add(categoriesPojo.getCategoryName());
            }
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subCatArray);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spSubcategory.setAdapter(dataAdapter);

    }

    public void getCategories() {
        categoriesList.clear();
        categoriesList = CategoryTable.getInstance().getMainCategories();
        if (categoriesList.size() <= 1) {
            if (Utils.isNetworkAvailable(this)) {
                //Utils.showProgressBar(this,progressBar);
                getCategoriesFromServer();
            } else {
                Utils.showSnackBar(this, rlMain, context.getString(R.string.no_internet));
            }
        } else {
            setCategoryAdapter();
            Utils.hideProgressBar(this, progressBar);
        }

    }

    //@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                //if (isFormValidated()) {
                Utils.hide_keyboard(this);
                if (Utils.isNetworkAvailable(this)) {
                    if (Utils.isPermissionGranted(RegisterActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        register();
                    } else {
                        Utils.requestPermissions(RegisterActivity.this, GPS_PERMISSION_CODE,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION);
                    }
                } else {
                    Utils.showSnackBar(this, rlMain, context.getString(R.string.no_internet));
                }
                //}
                break;
            case R.id.til_dob:
                DatePickerDialog datePicker = new DatePickerDialog(RegisterActivity.this,
                        date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePicker.show();
                break;

            case R.id.et_dob:
                DatePickerDialog datePicker2 = new DatePickerDialog(RegisterActivity.this,
                        date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePicker2.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePicker2.show();
                break;

        }
    }

    // @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GPS_PERMISSION_CODE:
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        if (i == permissions.length - 1) {
                            if(gpsLocation == null){
                                gpsLocation = new GPSLocation(RegisterActivity.this);
                            }
                            gpsLocation.init();
                            /*if (isFormValidated()) {
                                if (Utils.isNetworkAvailable(this)) {
                                    register();
                                } else {
                                    Utils.showSnackBar(this, rlMain, context.getString(R.string.no_internet));
                                }
                            }*/
                        }
                    } else {
                        if (i == permissions.length - 1) {
                            finish();
                        }
                    }
                }
                break;
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.sp_category:
                if (catArray.size() <= 1) {
                    getCategories();
                }
                break;
        }
        return false;
    }

    public String getGender() {
        int selectedId = rgGender.getCheckedRadioButtonId();
        rbGender = (RadioButton) findViewById(selectedId);
        return rbGender.getText().toString();
    }


    public String getDob() {
        if (TextUtils.isEmpty(etDob.getText().toString())) {
            return "";
        } else {
            return etDob.getText().toString();
        }
    }

    private boolean isFormValidated() {
        if (TextUtils.isEmpty(etName.getText().toString())) {
            tilName.setErrorEnabled(false);
            tilName.setError(null);
            tilName.requestFocus();
            tilName.setError(getResources().getString(R.string.error_enter_name));
            return false;
        } else {
            tilName.setErrorEnabled(false);
            tilName.setError(null);
        }

        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            tilEmail.setErrorEnabled(false);
            tilEmail.setError(null);
            tilEmail.requestFocus();
            tilEmail.setError(getResources().getString(R.string.error_enter_email_id));
            return false;
        } else if (!Utils.isValidMail(etEmail.getText().toString())) {
            tilEmail.setErrorEnabled(false);
            tilEmail.setError(null);
            tilEmail.requestFocus();
            tilEmail.setError(getResources().getString(R.string.error_invalid_email_id));
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
            tilEmail.setError(null);
        }


        if (TextUtils.isEmpty(etPwd.getText().toString())) {
            tilPwd.setErrorEnabled(false);
            tilPwd.setError(null);
            tilPwd.requestFocus();
            tilPwd.setError(getResources().getString(R.string.error_enter_pwd));
            return false;
        } else {
            tilPwd.setErrorEnabled(false);
            tilPwd.setError(null);
        }

        if (TextUtils.isEmpty(etConfPwd.getText().toString())) {
            tilConfPwd.setErrorEnabled(false);
            tilConfPwd.setError(null);
            tilConfPwd.requestFocus();
            tilConfPwd.setError(getResources().getString(R.string.error_enter_conf_pwd));
            return false;
        } else {
            tilConfPwd.setErrorEnabled(false);
            tilConfPwd.setError(null);
        }

        if (!TextUtils.isEmpty(etPwd.getText().toString()) && !TextUtils.isEmpty(etConfPwd.getText().toString())) {
            if (!etPwd.getText().toString().equalsIgnoreCase(etConfPwd.getText().toString())) {
                tilConfPwd.setErrorEnabled(false);
                tilConfPwd.setError(null);
                tilConfPwd.requestFocus();
                tilConfPwd.setError(getResources().getString(R.string.error_pwd));
                return false;
            } else {
                tilConfPwd.setErrorEnabled(false);
                tilConfPwd.setError(null);
            }
        }

        if (TextUtils.isEmpty(etPhone.getText().toString())) {
            tilPhone.setErrorEnabled(false);
            tilPhone.setError(null);
            tilPhone.requestFocus();
            tilPhone.setError(getResources().getString(R.string.error_enter_phone_number));
            return false;
        } else {
            tilPhone.setErrorEnabled(false);
            tilPhone.setError(null);
        }

        if (catId == null) {
            Utils.showSnackBar(RegisterActivity.this, rlMain, getResources().getString(R.string.error_select_cat));
            return false;
        }

        if (subCategoriesList.size() > 0) {
            if (subCatId == null) {
                Utils.showSnackBar(RegisterActivity.this, rlMain, getResources().getString(R.string.error_select_subcat));
                return false;
            }
        }

        return true;
    }

    public void register() {
        String email = etEmail.getText().toString().trim();
        String name = etName.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String pwd = etPwd.getText().toString().trim();
        //String gender = getGender();
        //String dob = getDob();
        String country = "India";/*(spCountry.getSelectedItem().toString().equalsIgnoreCase(Constants.SELECT_COUNTRY)
                                ? "" : spCountry.getSelectedItem().toString());*/
        String cat = (catId != null ? catId : "0");/*(spCategory.getSelectedItem().toString().equalsIgnoreCase(Constants.SELECT_CATEGORY)
                            ? "" : spCategory.getSelectedItem().toString());*/
        String subCat = (subCatId != null ? subCatId : "");/*(spSubcategory.getSelectedItem() != null) ? (spSubcategory.getSelectedItem().toString().equalsIgnoreCase(Constants.SELECT_SUBCATEGORY)
                                ? "" : spSubcategory.getSelectedItem().toString()) : "";*/

        String desc = etDescription.getText().toString();
        String website = etWebsite.getText().toString();
        String zipcode = etZipcode.getText().toString();
        /*String lat = Preferences.getStringPreference(this, Constants.LAT_LAST);
        String longitude = Preferences.getStringPreference(this, Constants.LONG_LAST);*/

        register(email, phone, name, pwd, country,
                cat, subCat, desc, website, zipcode);
    }

    private String setDay(String date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date dateformat = null;
        try {
            dateformat = (Date) formatter.parse(date);
            // System.out.println("dte"+dateformat);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String dt = String.valueOf(dateformat);
        //  System.out.println("Folder Name2 = " + dt);

        SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");
        String strdate = sdf.format(dateformat);

        return strdate;
    }

    private void removeError() {

        tilName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString())) {
                    tilName.setErrorEnabled(false);
                    tilName.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tilEmail.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString())) {
                    tilEmail.setErrorEnabled(false);
                    tilEmail.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tilPwd.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString())) {
                    tilPwd.setErrorEnabled(false);
                    tilPwd.setError(null);
                    tilConfPwd.setErrorEnabled(false);
                    tilConfPwd.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tilConfPwd.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString())) {
                    tilConfPwd.setErrorEnabled(false);
                    tilConfPwd.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tilPhone.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString())) {
                    tilPhone.setErrorEnabled(false);
                    tilPhone.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private ArrayList<CountryPojo> settingCountry() {

        ArrayList result = new ArrayList<CountryPojo>();
        try {
            JSONObject jsonObject = new JSONObject(loadTextFromFolder());

            JSONArray jsonArray = jsonObject.getJSONArray("Data");
            //countryComing = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject insideJson = jsonArray.getJSONObject(i);
                String countryName = insideJson.getString("country");
                String countryCode = insideJson.getString("id");

                CountryPojo countryPojo = new CountryPojo(countryName);
                result.add(countryPojo);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Collections.sort(result, new Comparator<CountryPojo>() {
            @Override
            public int compare(CountryPojo countryPojo1, CountryPojo countryPojo2) {
                return countryPojo1.getName().compareTo(countryPojo2.getName());
            }
        });
        return result;
    }

    public String loadTextFromFolder() {
        AssetManager assetManager = getAssets();
        String bufferString = "";
        try {
            InputStream is = assetManager.open("country.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            bufferString = new String(buffer);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bufferString;
    }

    private void register(String email, String mobile, String name, final String pwd,
                          String country, final String cat, String subCat,
                          String desc, final String website, String zipcode) {
        Utils.showProgressBar(RegisterActivity.this, progressBar);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);
        params.put(Constants.USER_MOBILE, mobile);
        params.put(Constants.USER_NAME, name);
        params.put(Constants.USER_EMAIL, email);
        params.put(Constants.USER_PWD, pwd);
        //params.put(Constants.USER_GENDER, gender);
        //params.put(Constants.USER_DOB, dob);
        params.put(Constants.USER_COUNTRY, country);
        params.put(Constants.DEVICE_ID, Utils.getAndroidId(this));
        params.put(Constants.REG_ID, "sdddasd");
        params.put(Constants.DEVICE_TYPE, Constants.ANDROID);
        params.put(Constants.USER_ID, Preferences.getStringPreference(this, Constants.USER_ID));

        if(gpsLocation == null) {
            params.put("lat", String.valueOf(latitude));
            params.put("long", String.valueOf(longitude));
        }else {
            params.put("lat", String.valueOf(gpsLocation.getLatitude()));
            params.put("long", String.valueOf(gpsLocation.getLognitude()));
        }
        /*if (lat.equals("") || longitude.equals("")) {
            params.put("lat", "0.0");
            params.put("long", "0.0");
        }else {
            params.put("lat", lat);
            params.put("long", longitude);
        }*/

        if (cat != null) {
            params.put(Constants.CATEGORY, cat);
        }

        if (subCat != null) {
            params.put(Constants.SUBCATEGORY, subCat);
        }

        if (!desc.equalsIgnoreCase("")) {
            params.put(Constants.DESCRIPTION, desc);
        }

        if (!website.equalsIgnoreCase("")) {
            params.put(Constants.WEBSITE, website);
        }

        if (!zipcode.equalsIgnoreCase("")) {
            params.put(Constants.ZIPCODE, zipcode);
        }

        for (String key : params.keySet()) {
            System.out.println("key : " + key);
            System.out.println("value : " + params.get(key));
        }

        CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_REGISTER, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response==" + response);
                    if (response.get("response_code").equals("1")) {
                        Utils.hideProgressBar(RegisterActivity.this, progressBar);
                        // To end Google Session
                        //  response=={"response_code":"1","message":"Success","data":{"id":"23","role_id":"0","user_mobile":"1234567890","user_email":"Test@test.com","username":"Test","name":"","first_name":"Test","last_name":"","user_dob":"2016-10-17","user_gender":"","user_pic":"","user_address":"","user_city":"","user_state":"","user_country":"","country_code":"","user_profile_status":"","login_status":"offline","note":"","creation_date":"0000-00-00 00:00:00","modification_date":"0000-00-00 00:00:00","user_id":"23"}}
                        JSONObject jsonObject = response.getJSONObject("data");
                        //Intent intent = new Intent(RegisterActivity.this, CategoriesActivity.class);
                        //startActivity(intent);
                        //finish();

                        etName.setText("");
                        etEmail.setText("");
                        etPhone.setText("");
                        etPwd.setText("");
                        etConfPwd.setText("");
                        catId = null;
                        subCatId = null;
                        spCategory.setSelection(0);
                        spSubcategory.setSelection(0);
                        etDescription.setText("");
                        etWebsite.setText("");
                        etZipcode.setText("");
                        Utils.showSnackBar(RegisterActivity.this, rlMain, response.getString("message"));
                    } else {
                        //Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                        Utils.hideProgressBar(RegisterActivity.this, progressBar);
                        Utils.showSnackBar(RegisterActivity.this, rlMain, response.getString("message"));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    //Toast.makeText(context, "Error in Login", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressBar(RegisterActivity.this, progressBar);
                    Utils.showSnackBar(RegisterActivity.this, rlMain, "Error in Login");
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                Utils.hideProgressBar(RegisterActivity.this, progressBar);
                Utils.showSnackBar(RegisterActivity.this, rlMain, error.toString());
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, Constants.URL_REGISTER);

    }

    public void getCategoriesFromServer() {
        if (catRequestCheck) {
            catRequestCheck = false;
            Utils.showProgressBar(this, progressBar);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);

            final CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_CATEGORY, params, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        catRequestCheck = true;
                        Log.i(TAG, response.toString());
                        if (response.get("status").equals(true)) {
                            Utils.hideProgressBar(RegisterActivity.this, progressBar);
                            categoriesList.clear();
                            JSONArray data = response.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                CategoriesPojo cat = new CategoriesPojo();
                                JSONObject category = (JSONObject) data.get(i);
                                String catName = category.getString("cat_name");
                                String catId = category.getString("cat_id");
                                String parentId = category.getString("parent_id");
                                String status = category.getString("status");
                                cat.setCategoryName(catName);
                                cat.setCateId(catId);
                                cat.setParentId(parentId);
                                cat.setStatus(status);
                                CategoryTable.getInstance().add(cat);
                                //categoriesList.add(cat);
                            }
                            setCategoryAdapter();
                        } else {
                            Utils.hideProgressBar(RegisterActivity.this, progressBar);
                        }
                    /*if (response.get("response_code").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("data");
                        String roleId = jsonObject.optString("role_id");
                    }*/
                    } catch (Exception e) {
                        catRequestCheck = true;
                        e.printStackTrace();
                        Utils.hideProgressBar(RegisterActivity.this, progressBar);
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    catRequestCheck = true;
                    error.printStackTrace();
                    Utils.hideProgressBar(RegisterActivity.this, progressBar);
                }

            });
            request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(request, Constants.URL_CATEGORY);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_category:
                if (position != 0) {
                    subCatId = null;
                    CategoriesPojo cat = categoriesList.get(position - 1);

                    ///String parentId = catHash.spCategory.getSelectedItem().toString();
                    //CategoriesPojo cat = (CategoriesPojo) parent.getItemAtPosition(position);
                    catId = cat.getCateId();
                    subCategoriesList.clear();
                    subCategoriesList = CategoryTable.getInstance().getSubCategories(cat.getCateId());
                    setSubcategoryAdapter();
                    //Log.i(TAG, new Gson().toJson(subCategoriesList));
                } else {
                    //subCategoriesList.clear();
                    //setSubcategoryAdapter();
                }
                break;
            case R.id.sp_sub_category:
                if (position != 0) {
                    subCatId = subCategoriesList.get(position - 1).getCateId();
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
