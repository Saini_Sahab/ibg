package com.ibghub.search.listing.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.fragment.CategoryFragment;
import com.ibghub.search.listing.fragment.ProfileFragment;
import com.ibghub.search.listing.fragment.SearchFragment;
import com.ibghub.search.listing.gps.GPSLocation;
import com.ibghub.search.listing.utility.Utils;

import java.util.List;

public class CategoriesActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private static final int GPS_PERMISSION_CODE = 5;
    public static ViewPager pager;
    static Toolbar toolbar;
    public GPSLocation gpsLocation;
    static CategoriesActivity instance;

    public static CategoriesActivity getInstance() {
        return instance;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(gpsLocation == null) {
            gpsLocation = new GPSLocation(CategoriesActivity.this);
        }
        gpsLocation.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        instance = this;
        settigToolBar();

        if (Utils.isPermissionGranted(this,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            if(gpsLocation == null){
                gpsLocation = new GPSLocation(CategoriesActivity.this);
            }
            gpsLocation.init();
            setPager();
        } else {
            Utils.requestPermissions(this, GPS_PERMISSION_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
        }

    }

    //@TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GPS_PERMISSION_CODE:
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        if (i == permissions.length - 1) {
                            if(gpsLocation == null){
                                gpsLocation = new GPSLocation(CategoriesActivity.this);
                            }
                            gpsLocation.init();
                            setPager();
                        }
                    } else {
                        if (i == permissions.length - 1) {
                            finish();
                        }
                    }
                }
                break;
        }
    }

    private void setPager() {
        pager = (ViewPager) findViewById(R.id.pager);

        /** set the adapter for ViewPager */
        pager.setAdapter(new SamplePagerAdapter(
                getSupportFragmentManager()));
        pager.addOnPageChangeListener(this);
        pager.setCurrentItem(1);
    }

    private void settigToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Categories");
        changeToolbarSetting(1);
    }

    //@RequiresApi(api = Build.VERSION_CODES.M)
    /*private void getLocation() {
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Preferences.setStringPreference(this, Constants.LAT_LAST, String.valueOf(latitude));
            Preferences.setStringPreference(this, Constants.LONG_LAST, String.valueOf(longitude));
            // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }*/

    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void changeToolbarSetting(final int position) {
        switch (position) {
            case 0:
                toolbar.setTitle("Profile");
                toolbar.setTitleTextColor(Color.WHITE);
                toolbar.setNavigationIcon(null);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setElevation(10);
                }
                toolbar.setBackgroundColor(Color.parseColor("#505BB625"));
                //toolbar.setBackgroundColor(this.getResources().getColor(R.color.colorPrimary));
                break;
            case 1:
                toolbar.setTitle("Categories");
                toolbar.setTitleTextColor(Color.WHITE);
                toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setElevation(10);
                }
                toolbar.setBackgroundColor(Color.parseColor("#505BB625"));
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pager.setCurrentItem(0);
                    }
                });
                break;

            case 2:
                toolbar.setTitle("Search");
                toolbar.setTitleTextColor(Color.WHITE);
                toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setElevation(10);
                }
                toolbar.setBackgroundColor(Color.parseColor("#505BB625"));
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        changeToolbarSetting(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * Defining a FragmentPagerAdapter class for controlling the fragments to be shown when user swipes on the screen.
     */
    public class SamplePagerAdapter extends FragmentPagerAdapter {

        public SamplePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            /** Show a Fragment based on the position of the current screen */
            switch (position) {
                case 0:
                    return ProfileFragment.newInstance();
                case 1:
                    return CategoryFragment.newInstance();
                case 2:
                    return new SearchFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 3;
        }
    }


    @Override
    public void onBackPressed() {
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        Fragment lastFragment = getLastNotNull(frags);
        if (pager.getCurrentItem() == 1) {
            super.onBackPressed();
            finish();
        } else if (lastFragment instanceof SearchFragment) {
            if (((SearchFragment) lastFragment).rlCategories.getVisibility() == View.VISIBLE) {
                ((SearchFragment) lastFragment).collapse();
            } else {
                pager.setCurrentItem(1);
            }
        } else if (lastFragment instanceof ProfileFragment) {
            pager.setCurrentItem(1);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    private Fragment getLastNotNull(List<Fragment> list) {
        for (int i = list.size() - 1; i > 0; i--) {
            Fragment frag = list.get(i);
            if (frag != null) {
                return frag;
            }
        }
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(gpsLocation != null) {
            if (Utils.isPermissionGranted(CategoriesActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                //getLocation();
                gpsLocation.onResume();
            /*LatLng latLng = new LatLng(gpsLocation.getLatitude(),gpsLocation.getLognitude());
            try {
                SearchFragment.getInstance().setMyLocation(latLng);
            }catch (Exception e){
                e.printStackTrace();
            }*/
                //latitude = gpsLocation.getLatitude();
                //longitude = gpsLocation.getLognitude();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(gpsLocation != null) {
            gpsLocation.onPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (gpsLocation != null) {
            gpsLocation.onStop();
        }
    }
}
