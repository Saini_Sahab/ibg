package com.ibghub.search.listing.activity;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.adapter.CategoryAdapter;
import com.ibghub.search.listing.adapter.SubCategoryAdapter;
import com.ibghub.search.listing.database.tables.CategoryTable;
import com.ibghub.search.listing.decorator.DividerItemDecoration;
import com.ibghub.search.listing.pojo.CategoriesPojo;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class SubCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvNoSubCat;
    RecyclerView rvSubcat;
    ImageView ivChangeGrid;
    SubCategoryAdapter subCategoryAdapter;
    GridLayoutManager layoutManager;

    boolean gridToggle = false;
    ArrayList<CategoriesPojo> subCategoriesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        bind();
        setAdapter();
    }

    public void bind(){
        tvNoSubCat = (TextView)findViewById(R.id.tv_no_sub_cat);
        rvSubcat = (RecyclerView)findViewById(R.id.rv_sub_cat);
        ivChangeGrid = (ImageView)findViewById(R.id.iv_change_grid);

        ivChangeGrid.setOnClickListener(this);
    }

    public void setAdapter(){
        if(subCategoriesList.size()==0){
            subCategoriesList = CategoryTable.getInstance().getMainCategories();
        }
        if(subCategoriesList.size()>0){
            tvNoSubCat.setVisibility(View.GONE);
        }else {
            tvNoSubCat.setVisibility(View.VISIBLE);
        }
        subCategoryAdapter = new SubCategoryAdapter(this, subCategoriesList);
        if(gridToggle){
            ivChangeGrid.setBackgroundResource(R.drawable.ic_grid1_black_50dp);
            //rvSubcat.setLayoutManager(new GridLayoutManager(this,2));
            layoutManager = new GridLayoutManager(this,2);
        }else {
            ivChangeGrid.setBackgroundResource(R.drawable.ic_grid2_black_50dp);
            //rvSubcat.setLayoutManager(new GridLayoutManager(this,1));
            layoutManager = new GridLayoutManager(this,1);
        }

        rvSubcat.setLayoutManager(layoutManager);
        //Drawable horizontalDivider = ContextCompat.getDrawable(this, R.drawable.bg);
        //recyclerView.addItemDecoration(new DividerItemDecoration(horizontalDivider,horizontalDivider,3));

        rvSubcat.setAdapter(subCategoryAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_change_grid:
                if(gridToggle) {
                    ivChangeGrid.setBackgroundResource(R.drawable.ic_grid2_black_50dp);
                    gridToggle = false;
                    layoutManager.setSpanCount(1);
                    //rvSubcat.setLayoutManager(new GridLayoutManager(this,1));
                }else {
                    ivChangeGrid.setBackgroundResource(R.drawable.ic_grid1_black_50dp);
                    gridToggle = true;
                    layoutManager.setSpanCount(2);
                    //rvSubcat.setLayoutManager(new GridLayoutManager(this,2));
                }
                break;
        }
    }
}
