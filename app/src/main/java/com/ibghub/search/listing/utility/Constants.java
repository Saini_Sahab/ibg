package com.ibghub.search.listing.utility;


public class Constants {
    public static final String VOLLY_AUTH_KEY = "CJW8DcHqx71C";
    public static final String BASE_URL = "http://app.ibghub.com/api/";
    public static final String URL_SIGN_IN = "signin";
    public static final String URL_USER_CREATE = "usercreate";
    public static final String URL_CATEGORY = "category";
    public static final String URL_REGISTER = "register";
    public static final String URL_LISTING = "listing";
    public static final String URL_OTP = "otp";

    public static final String USER_ID = "user_id";

    public static final String GCM_REG_ID = "GCM_REG_ID";

    public static final String KEY = "key";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PWD = "user_pass";
    public static final String DEVICE_ID = "device_id";
    public static final String REG_ID = "reg_id";
    public static final String DEVICE_TYPE = "device_type";
    public static final String ANDROID = "android";
    public static final String USER_NAME = "name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_GENDER = "user_gender";
    public static final String USER_DOB = "user_dob";
    public static final String USER_COUNTRY = "user_country";
    public static final String SELECT_COUNTRY = "Select Country";
    public static final String SELECT_STATE = "Select State";
    public static final String SELECT_CITY = "Select City";
    public static final String SELECT_CATEGORY = "Select Category";
    public static final String SELECT_SUBCATEGORY = "Select Subcategory";
    public static final String CATEGORY = "category";
    public static final String CAT_ID = "cat_id";
    public static final String SUB_CAT_ID = "sub_cat_id";
    public static final String SUBCATEGORY = "sub_category";
    public static final String DESCRIPTION = "description";
    public static final String WEBSITE = "website";
    public static final String ZIPCODE = "zipcode";
    public static final String USER_TYPE = "list";
    public static final String LAT_LAST = "last_lat";
    public static final String LONG_LAST = "last_long";

    public static final String LOGIN = "login";
    public static final String LOGIN_AS = "login_as";
    //Permissions request code
    public static final int PERMISSIONS_GPS = 101;

    public static final String OTP = "otp";


}
