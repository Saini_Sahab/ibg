package com.ibghub.search.listing.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.pojo.CategoriesPojo;

import java.util.ArrayList;

/**
 * Created by Lenovo on 06-Nov-16.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {

    Context context;
    ArrayList<CategoriesPojo> categoriesList = new ArrayList<>();

    public SubCategoryAdapter(Context context, ArrayList<CategoriesPojo> categoriesList) {
        this.context = context;
        this.categoriesList = categoriesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_layout_sub_categories, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CategoriesPojo subCat = categoriesList.get(position);

        holder.tvSubCatName.setText(subCat.getCategoryName());

    }

    @Override
        public int getItemCount() {
        return categoriesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSubCatName;
        private ImageView ivSubCatImage;

        public ViewHolder(View itemView) {
            super(itemView);

            tvSubCatName = (TextView)itemView.findViewById(R.id.tv_sub_cat_name);
            ivSubCatImage =(ImageView)itemView.findViewById(R.id.iv_sub_cat_image);
        }
    }
}
