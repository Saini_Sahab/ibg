package com.ibghub.search.listing.fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ibghub.search.listing.R;
import com.ibghub.search.listing.activity.CategoriesActivity;
import com.ibghub.search.listing.adapter.ExpandableListAdapter;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.database.tables.CategoryTable;
import com.ibghub.search.listing.pojo.CategoriesPojo;
import com.ibghub.search.listing.pojo.ListingPojo;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Utils;
import com.ibghub.search.listing.utility.UtilsServer;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchFragment extends Fragment implements SearchView.OnQueryTextListener, OnMapReadyCallback {

    private static final int GPS_PERMISSION_CODE = 5;
    GoogleMap mMap;
    public RelativeLayout rlCategories;
    private ValueAnimator mAnimator;
    private ExpandableListView expandableListView;
    ProgressBar progressBar;
    RelativeLayout rlMain;

    ArrayList<CategoriesPojo> categoriesList = new ArrayList<>();
    private ArrayList<CategoriesPojo> subList;
    private ArrayList<CategoriesPojo> expandableListTitle;
    private HashMap<String, ArrayList<CategoriesPojo>> expandableListDetail;
    ExpandableListAdapter expandableListAdapter;
    private int lastExpandedPosition = -1;
    ArrayList<ListingPojo> searchedCatagories;
    ArrayList<LatLng> latlngList;
    public static SearchFragment instance;
    LatLng latLng;
    String address = "";
    ArrayList<String> addressList;

    private int[] catMarkers = {R.drawable.ic_marker_plumber,
            R.drawable.ic_marker_drycleaner,
            R.drawable.ic_marker_placement,
            R.drawable.ic_marker_coaching,
            R.drawable.ic_marker_tourntravel,
            R.drawable.ic_marker_hotels,
            R.drawable.ic_marker_doctor,
            R.drawable.ic_marker_repairing,
            R.drawable.ic_marker_electronics};

    public SearchFragment() {
        // Required empty public constructor
        instance = this;
    }

    public static SearchFragment getInstance() {
        return instance;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        instance = this;
        inflater.inflate(R.menu.searchfragment_menu, menu);
        /*final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchItem.setTitle("Search");
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(Color.WHITE);
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setTextColor(Color.WHITE);
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHint("Search");

        searchView.setOnQueryTextListener(this);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_cat:
                if (rlCategories.getVisibility() == View.GONE) {
                    expand();
                } else {
                    collapse();
                    try {
                        expandableListView.collapseGroup(lastExpandedPosition);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        setHasOptionsMenu(true);
        findViews(view);
        handler.post(runnable);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (mMap != null) {
                CategoriesActivity.getInstance().gpsLocation.onResume();
                latLng = new LatLng(CategoriesActivity.getInstance().gpsLocation.getLatitude(),
                        CategoriesActivity.getInstance().gpsLocation.getLognitude());
                if (latlngList.size() > 0) {
                    latlngList.remove(0);
                }
                latlngList.add(0, latLng);
                new GetAddress().execute(latlngList);
            }
            //setMyLocation(latLng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //AppController.getInstance().addUIListener(UiListener.class, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //AppController.getInstance().removeUIListener(UiListener.class, this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /**
         * map fragment can only be added dynamically in another fragment
         */
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            ft.add(R.id.mapFragmentContainer, mapFragment, "mapFragment");
            ft.commit();
            getChildFragmentManager().executePendingTransactions();
        }
        mapFragment.getMapAsync(this);
    }


    public void findViews(View view) {
        rlMain = (RelativeLayout) getActivity().findViewById(R.id.activity_categories);
        rlCategories = (RelativeLayout) view.findViewById(R.id.rl_categories);
        expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        expandableListDetail = new HashMap<String, ArrayList<CategoriesPojo>>();
        expandableListTitle = new ArrayList<>();
        searchedCatagories = new ArrayList<>();
        addressList = new ArrayList<>();
        latlngList = new ArrayList<>();

        categoriesList = CategoryTable.getInstance().getMainCategories();
        if (categoriesList.size() == 0) {
            if (Utils.isNetworkAvailable(getActivity())) {
                UtilsServer.getCategories();
            } else {
                Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            for (CategoriesPojo categoryList : categoriesList) {
                ArrayList subList = CategoryTable.getInstance().getSubCategories(categoryList.getCateId());
                ArrayList<CategoriesPojo> arrayList = new ArrayList<CategoriesPojo>();
                expandableListDetail.put(categoryList.getCategoryName(), subList);
                expandableListTitle.add(categoryList);
            }
            expandableListAdapter = new ExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail);
            expandableListView.setAdapter(expandableListAdapter);
        }

        rlCategories.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        rlCategories.getViewTreeObserver().removeOnPreDrawListener(this);
                        rlCategories.setVisibility(View.GONE);
                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(
                                0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        rlCategories.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(0, (categoriesList.size() > 5)
                                ? rlCategories.getMeasuredHeight() * 5
                                : rlCategories.getMeasuredHeight() * categoriesList.size());
                        return true;
                    }
                });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (Utils.isNetworkAvailable(getActivity())) {
                    searchedCatagories.clear();
                    CategoriesPojo subCat = (CategoriesPojo) expandableListAdapter.getChild(groupPosition, childPosition);
                    listing(subCat.getParentId(), subCat.getCateId());
                    collapse();
                    expandableListView.collapseGroup(lastExpandedPosition);
                } else {
                    Utils.showSnackBar(getActivity(), rlMain, getActivity().getString(R.string.no_internet));
                }
                //startActivity(new Intent(getActivity(), SubmitQuery.class).putExtra("DESCRIPTION", allist.get(childPosition)).putExtra("CAT_NAME", expandableListTitle.get(groupPosition).getCategoryName()).putExtra("CAT_ID", expandableListTitle.get(groupPosition).getCategoryName()).putExtra("CAT_SUB_TITLE", pic.getSubName()));
                return false;
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            this.mMap = googleMap;

            if (Utils.isPermissionGranted(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                latLng = new LatLng(CategoriesActivity.getInstance().gpsLocation.getLatitude(),
                        CategoriesActivity.getInstance().gpsLocation.getLognitude());
                latlngList.clear();
                addressList.clear();
                latlngList.add(latLng);
                new GetAddress().execute(latlngList);
                //setMyLocation(latLng);
            } else {
                Utils.requestPermissions(getActivity(), GPS_PERMISSION_CODE,
                        Manifest.permission.ACCESS_FINE_LOCATION);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*@Override
    public void update(String type, Object data) {
        if (Utils.isPermissionGranted(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            LatLng latLng = new LatLng(((Location) data).getLatitude(), ((Location) data).getLongitude());
            setMyLocation(latLng);
        } else {
            Utils.requestPermissions(getActivity(), GPS_PERMISSION_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }*/

    public void setMyLocation(LatLng latLng) {
        this.latLng = latLng;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        // LatLng TutorialsPoint = new LatLng(21, 57);

        /*if (latLng == null) {
            if (!Preferences.getStringPreference(getActivity(), Constants.LAT_LAST).equalsIgnoreCase("")
                    && !Preferences.getStringPreference(getActivity(), Constants.LONG_LAST).equalsIgnoreCase("")) {
                LatLng current = new LatLng(Double.parseDouble(Preferences.getStringPreference(getActivity(), Constants.LAT_LAST)),
                        Double.parseDouble(Preferences.getStringPreference(getActivity(), Constants.LONG_LAST)));
                latLng = current;
                setMyLocationMarker(latLng);
            }
        } else {*/
        mMap.clear();
        setMyLocationMarker(latLng);
        if (searchedCatagories.size() > 0) {
            setSearchedCatMarker(searchedCatagories);
        }
        //}
    }

    private void setMyLocationMarker(LatLng latLng) {

        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();
        //mMap.clear();
        if (addressList.size() > 0) {
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(addressList.get(0)));
        }
        //.snippet(address));
        // .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_close)));
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater(Bundle.EMPTY).inflate(R.layout.map_marker_info_layout, null);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());


                return infoWindow;
            }
        });
    }

    private void setSearchedCatMarker(ArrayList<ListingPojo> listingMarkers) {
        //mMap.clear();
        //for (ListingPojo list : listingMarkers) {
        for (int i = 0; i < listingMarkers.size(); i++) {
            ListingPojo list = listingMarkers.get(i);
            String catAddress = addressList.get(i + 1);
            LatLng latLng = new LatLng(Double.parseDouble(list.getLatitude()), Double.parseDouble(list.getLongitude()));
            String title = list.getTitle();
            String phone = list.getPhone();

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();


            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(title + ", " + phone)
                    .snippet(catAddress)
                    .icon(BitmapDescriptorFactory.fromResource(catMarkers[Integer.parseInt(list.getCatId().toString()) - 1])));
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


                @Override
                // Return null here, so that getInfoContents() is called next.
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    // Inflate the layouts for the info window, title and snippet.
                    View infoWindow = getLayoutInflater(Bundle.EMPTY).inflate(R.layout.map_marker_info_layout, null);

                    TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                    title.setText(marker.getTitle());

                    TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                    snippet.setText(marker.getSnippet());


                    return infoWindow;
                }
            });
        }
    }


    private void expand() {
        // set Visible
        rlCategories.setVisibility(View.VISIBLE);
        mAnimator.start();
    }

    public void collapse() {
        int finalHeight = rlCategories.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                // Height=0, but it set visibility to GONE
                rlCategories.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                // Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = rlCategories.getLayoutParams();
                layoutParams.height = value;
                rlCategories.setLayoutParams(layoutParams);
            }
        });
        return animator;

    }

    class GetAddress extends AsyncTask<ArrayList<LatLng>, Void, ArrayList<String>> {
        private Context context;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<String> doInBackground(ArrayList<LatLng>... params) {
            ArrayList<String> address = new ArrayList<>();
            try {
                for (LatLng latLng : params[0]) {
                    if (latLng.longitude != 0.0 && latLng.latitude != 0.0) {
                        URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng.latitude + "," + latLng.longitude + "&sensor=true");
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                        InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                        StringBuilder builder = new StringBuilder();

                        String inputString;
                        while ((inputString = bufferedReader.readLine()) != null) {
                            builder.append(inputString);
                        }
                        address.add(builder.toString());
                        urlConnection.disconnect();
                    } else {
                        address.add(null);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return address;

        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            if (list != null) {
                addressList.clear();
                try {
                    for (String address : list) {
                        if (address != null) {
                            System.out.println("location====" + address);
                            JSONObject mainJsonObject = new JSONObject(address);
                            JSONArray ResultsJsonArray = mainJsonObject.getJSONArray("results");
                            JSONObject jsonObject = ResultsJsonArray.getJSONObject(1);
                            JSONArray address_components = jsonObject.getJSONArray("address_components");
                            JSONArray addressObject = address_components;
                            String detailAddress = addressObject.getJSONObject(2).optString("short_name")
                                    + "," + addressObject.getJSONObject(3).optString("short_name")
                                    + "," + addressObject.getJSONObject(4).optString("short_name");
                            addressList.add(detailAddress);
                        } else {
                            addressList.add("No Address Found");
                        }
                        //autoCompleteTextView.setText("" + address);
                    }
                    setMyLocation(latLng);
                } catch (JSONException e) {
                    //addressList.add("");
                    e.printStackTrace();
                }
            }

        }

    }

    public void listing(String catId, String subCatId) {
        Utils.showProgressBar(getActivity(), progressBar);
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);
        params.put(Constants.CAT_ID, catId);
        if (subCatId != null) {
            params.put(Constants.SUB_CAT_ID, subCatId);
        }
        /*params.put(Constants.DEVICE_ID, Utils.getAndroidId(context));
        params.put(Constants.REG_ID, "sdddasd");
        params.put(Constants.DEVICE_TYPE, Constants.ANDROID);*/


        CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_LISTING, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response==" + response);
                    if (response.get("status").equals(true)) {
                        Utils.hideProgressBar(getActivity(), progressBar);
                        // To end Google Session
                        if (response.has("data")) {
                            JSONArray data = response.getJSONArray("data");
                            /*for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = (JSONObject) data.get(i);
                                ListingPojo list = new Gson().fromJson(jsonObject.toString(),ListingPojo.class);
                                listingArrayList.add(list);
                            }*/

                            if (data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) data.getJSONObject(i);
                                    ListingPojo listingPojo = new ListingPojo();
                                    listingPojo.setLatitude(jsonObject.getString("latitude"));
                                    listingPojo.setLongitude(jsonObject.getString("longitude"));
                                    listingPojo.setTitle(jsonObject.getString("title"));
                                    listingPojo.setPhone(jsonObject.getString("phone"));
                                    listingPojo.setCatId(jsonObject.getString("cat_id"));
                                    searchedCatagories.add(listingPojo);
                                }
                                //setMyLocation(latLng);
                                latlngList.clear();
                                latlngList.add(latLng);
                                if (searchedCatagories.size() > 0) {
                                    //setSearchedCatMarker(searchedCatagories);
                                    for (ListingPojo listingPojo : searchedCatagories) {
                                        LatLng latLng = new LatLng(Double.parseDouble(listingPojo.getLatitude()),
                                                Double.parseDouble(listingPojo.getLongitude()));
                                        latlngList.add(latLng);
                                    }
                                }

                                new GetAddress().execute(latlngList);

                            } else {
                                Utils.showSnackBar(getActivity(), rlMain, "No Listing Available");
                            }
                        } else {
                            Utils.showSnackBar(getActivity(), rlMain, "No Data Available");
                        }

                    } else {
                        Utils.hideProgressBar(getActivity(), progressBar);
                        Utils.showSnackBar(getActivity(), rlMain, response.getString("message"));
                        //Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    //Toast.makeText(context, "Error in Login", Toast.LENGTH_SHORT).show();
                    Utils.hideProgressBar(getActivity(), progressBar);
                    Utils.showSnackBar(getActivity(), rlMain, "Error in Login");
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show();
                Utils.hideProgressBar(getActivity(), progressBar);
                Utils.showSnackBar(getActivity(), rlMain, error.toString());
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, Constants.URL_LISTING);

    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(latLng != null) {
                if (latLng.latitude == 0.0 || latLng.longitude == 0.0) {
                    if (mMap != null) {
                        CategoriesActivity.getInstance().gpsLocation.onResume();
                        latLng = new LatLng(CategoriesActivity.getInstance().gpsLocation.getLatitude(),
                                CategoriesActivity.getInstance().gpsLocation.getLognitude());
                        if (latlngList.size() > 0) {
                            latlngList.remove(0);
                        }
                        latlngList.add(0, latLng);
                        new GetAddress().execute(latlngList);
                    }handler.postDelayed(runnable, 1000);
                }else {
                    handler.removeCallbacks(runnable);
                }
            }else {
                handler.postDelayed(runnable, 1000);
            }
        }
    };

}
