package com.ibghub.search.listing.utility;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibghub.search.listing.activity.RegisterActivity;
import com.ibghub.search.listing.app.AppController;
import com.ibghub.search.listing.database.tables.CategoryTable;
import com.ibghub.search.listing.pojo.CategoriesPojo;
import com.ibghub.search.listing.volly.CustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.R.attr.category;

/**
 * Created by Lenovo on 05-Nov-16.
 */

public class UtilsServer {
    public static final String TAG = "UtilsServer";

    public static void getCategories() {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.KEY, Constants.VOLLY_AUTH_KEY);

        CustomRequest request = new CustomRequest(Request.Method.POST, Constants.BASE_URL + Constants.URL_CATEGORY, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.i(TAG, response.toString());
                    if (response.get("status").equals(true)) {
                        JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            CategoriesPojo cat = new CategoriesPojo();
                            JSONObject category = (JSONObject) data.get(i);
                            //JsonParser jsonParser = new JsonParser();
                            //JsonObject gsonObject = (JsonObject)jsonParser.parse(category.toString());
                            //CategoriesPojo cat = new Gson().fromJson(gsonObject,CategoriesPojo.class);
                            String catName = category.getString("cat_name");
                            String catId = category.getString("cat_id");
                            String parentId = category.getString("parent_id");
                            String status = category.getString("status");

                            cat.setCategoryName(catName);
                            cat.setCateId(catId);
                            cat.setParentId(parentId);
                            cat.setStatus(status);

                            CategoryTable.getInstance().add(cat);
                        }
                    }
                    /*if (response.get("response_code").equals("1")) {
                        JSONObject jsonObject = response.getJSONObject("data");
                        String roleId = jsonObject.optString("role_id");
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request, Constants.URL_CATEGORY);

    }
}
