package com.ibghub.search.listing.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.adapter.PagerAdapterImageSlider;
import com.ibghub.search.listing.pojo.ListingPojo;
import com.ibghub.search.listing.utility.Utils;

import static android.R.id.list;

public class ListingDetailActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    ViewPager viewPager;
    PagerAdapterImageSlider pagerAdapter;
    ImageView leftArrow, rightArrow;
    TextView tvName, tvAddress, tvPhone, tvTiming, tvDescription;
    Toolbar toolbar;
    Button btnWebSite, btnDirection;
    RelativeLayout main;

    ListingPojo listing = new ListingPojo();

    int[] images = new int[]{R.drawable.supedrycleaners, R.drawable.mainbanner,
            R.drawable.supedrycleaners, R.drawable.mainbanner,
            R.drawable.supedrycleaners, R.drawable.mainbanner,
            R.drawable.supedrycleaners, R.drawable.mainbanner};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_detail);

        if (getIntent().hasExtra("list_detail")) {
            listing = (ListingPojo) getIntent().getSerializableExtra("list_detail");

        }
        settigToolBar();
        bind();
        setValues();
    }

    public void bind() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new PagerAdapterImageSlider(ListingDetailActivity.this, images);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(this);

        tvName = (TextView) findViewById(R.id.tv_listing_name);
        tvAddress = (TextView) findViewById(R.id.tv_listing_address);
        tvPhone = (TextView) findViewById(R.id.tv_listing_phone);
        tvTiming = (TextView) findViewById(R.id.tv_listing_hours);
        tvDescription = (TextView) findViewById(R.id.tv_listing_description);

        leftArrow = (ImageView) findViewById(R.id.iv_left);
        rightArrow = (ImageView) findViewById(R.id.iv_right);
        btnWebSite = (Button) findViewById(R.id.btn_listing_website);
        main = (RelativeLayout) findViewById(R.id.activity_listing_detail);
        btnDirection = (Button) findViewById(R.id.btn_listing_direction);

        leftArrow.setOnClickListener(this);
        rightArrow.setOnClickListener(this);
        btnWebSite.setOnClickListener(this);
        btnDirection.setOnClickListener(this);

        setArrowVisibility();
    }

    private void settigToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Listing Detail");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setValues() {
        if (listing.getTitle() != null) {
            tvName.setText(listing.getTitle());
        }

        if (listing.getPhone() != null) {
            tvPhone.setText("Phone : " + listing.getPhone());
        }

        if (listing.getDescription() != null) {
            tvDescription.setText(Html.fromHtml(listing.getDescription()));
        }

        String time = "";
        if (listing.getOpenTime() != null) {
            if (!listing.getOpenTime().equalsIgnoreCase("")) {
                time = "Open today : " + listing.getOpenTime();
            }
        }

        if (listing.getCloseTime() != null) {
            if (!listing.getOpenTime().equalsIgnoreCase("")) {
                time += " - " + listing.getCloseTime();
            }
        }

        if (!time.equalsIgnoreCase("")) {
            tvTiming.setText(time);
        }

        String address = "";

        if (listing.getCountry() != null) {
            if (!listing.getCountry().equals("")) {
                address = "Adddress : " + listing.getCountry();
            }
        }

        if (listing.getState() != null) {
            if (!listing.getState().equals("")) {
                if (address.equals("")) {
                    address = "Adddress : " + listing.getState();
                } else {
                    address += " ," + listing.getState();
                }
            }
        }

        if (listing.getCity() != null) {
            if (!listing.getCity().equals("")) {
                if (address.equals("")) {
                    address = "Adddress : " + listing.getCity();
                } else {
                    address += " ," + listing.getCity();
                }
            }
        }

        if (listing.getZipCode() != null) {
            if (!listing.getZipCode().equals("")) {
                if (address.equalsIgnoreCase("")) {
                    address = "Address : " + listing.getZipCode();
                } else {
                    address += " ," + listing.getZipCode();
                }
            }
        }

        if (!address.equalsIgnoreCase("")) {
            tvAddress.setText(address);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                }
                setArrowVisibility();
                break;
            case R.id.iv_right:
                if (viewPager.getCurrentItem() != images.length - 1) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
                setArrowVisibility();
                break;

            case R.id.btn_listing_website:

                if (listing.getWebsite() == null) {
                    Utils.showSnackBar(this, main, "No Website Available");
                } else if (listing.getWebsite().equalsIgnoreCase("")) {
                    Utils.showSnackBar(this, main, "No Website Available");
                } else if (Utils.isNetworkAvailable(this)) {
                    Intent intent = new Intent(ListingDetailActivity.this, WebActivity.class);
                    intent.putExtra("website", listing.getWebsite().toString());
                    startActivity(intent);
                } else {
                    Utils.showSnackBar(this, main, this.getString(R.string.no_internet));
                }
                break;

            case R.id.btn_listing_direction:
                if(listing.getLatitude().equalsIgnoreCase("") && listing.getLongitude().equalsIgnoreCase("")){
                    Utils.showSnackBar(this, main, "No Location Available");
                }else {
                    Intent intent = new Intent(ListingDetailActivity.this, DirectionActivity.class);
                    intent.putExtra("list_detail",listing);
                    startActivity(intent);
                }
                break;
        }
    }

    public void setArrowVisibility() {
        if (images.length <= 0) {
            leftArrow.setVisibility(View.GONE);
            rightArrow.setVisibility(View.GONE);
        }

        if (images.length > 0) {
            if (viewPager.getCurrentItem() == 0) {
                leftArrow.setVisibility(View.GONE);
                rightArrow.setVisibility(View.VISIBLE);
            }

            if (viewPager.getCurrentItem() == images.length - 1) {
                leftArrow.setVisibility(View.VISIBLE);
                rightArrow.setVisibility(View.GONE);
            }

            if (viewPager.getCurrentItem() != 0 && viewPager.getCurrentItem() != images.length - 1) {
                leftArrow.setVisibility(View.VISIBLE);
                rightArrow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setArrowVisibility();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
