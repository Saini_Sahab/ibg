package com.ibghub.search.listing.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lenovo on 31-Oct-16.
 */


public class CategoriesPojo {

    @SerializedName("cat_id")
    private String cateId;

    @SerializedName("cat_name")
    private String categoryName;

    @SerializedName("parent_id")
    private String parentId;

    @SerializedName("status")
    private String status;

    private int categoryImage;


    public CategoriesPojo(String catId, String categoryName, int categoryImage) {
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
        this.cateId = catId;
    }

    public CategoriesPojo() {

    }

    public CategoriesPojo(String cateId, String categoryName, String parentId, String status) {
        this.cateId = cateId;
        this.categoryName = categoryName;
        this.parentId = parentId;
        this.status = status;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(int categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCateId() {
        return cateId;
    }

    public void setCateId(String cateId) {
        this.cateId = cateId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
