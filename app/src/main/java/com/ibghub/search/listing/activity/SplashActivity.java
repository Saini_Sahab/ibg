package com.ibghub.search.listing.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ibghub.search.listing.R;
import com.ibghub.search.listing.gps.GPSTracker;
import com.ibghub.search.listing.utility.Constants;
import com.ibghub.search.listing.utility.Preferences;
import com.ibghub.search.listing.utility.UtilsServer;

public class SplashActivity extends Activity {
  /*  private GoogleCloudMessaging gcmObj;
    private Context mContext;
    private String regId = "";*/
    GPSTracker gps;

    //@RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utils.setFullScreen(this);
        setContentView(R.layout.activity_splash);
        UtilsServer.getCategories();

        /*gps = new GPSTracker(SplashActivity.this);

        // check if GPS enabled
        if(gps.canGetLocation()){

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Preferences.setStringPreference(this,Constants.LAT_LAST,String.valueOf(latitude));
            Preferences.setStringPreference(this,Constants.LONG_LAST,String.valueOf(longitude));

            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        }*/

        //Utils.permission(this,Constants.PERMISSIONS_GPS, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Create an Intent that will start the Menu-Activity.
                if(Preferences.getBooleanPreference(SplashActivity.this,Constants.LOGIN)){
                    if(!Preferences.getStringPreference(SplashActivity.this,Constants.OTP).equals("")){
                        startActivity(new Intent(SplashActivity.this, OtpActivity.class));
                    }
                    else if(Preferences.getIntPreference(SplashActivity.this,Constants.LOGIN_AS)==1) {
                        startActivity(new Intent(SplashActivity.this, CategoriesActivity.class));
                    }
                    else if(Preferences.getIntPreference(SplashActivity.this,Constants.LOGIN_AS)==2) {
                        startActivity(new Intent(SplashActivity.this, RegisterActivity.class));
                    }
                }else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
                finish();
            }
        }, 3000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case Constants.PERMISSIONS_GPS:
                for(int i=0;i<permissions.length;i++){
                    if(grantResults[i]== PackageManager.PERMISSION_GRANTED){
                        Log.i("Permissions","Permission_Granted: "+ permissions[i]);

                    }
                    if(grantResults[i]==PackageManager.PERMISSION_DENIED){
                        Log.i("Permissions","Permission_Denied: "+ permissions[i]);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

  /*  private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcmObj == null) {
                        gcmObj = GoogleCloudMessaging
                                .getInstance(mContext);
                    }
                    regId = gcmObj.register(GOOGLE_PROJ_ID);
                    msg = "Registration ID :" + regId;
                    Log.e("Kaushal", "Reg id;;;;;;;;;;;;;;;;;;;;;;;====" + regId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regId)) {
                    Preferences.setStringPreference(mContext, GCM_REG_ID, regId);
//                    Toast.makeText(
//                            mContext,
//                            "Registered with GCM Server successfully.\n\n"
//                                    + msg, Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(
//                            mContext,
//                            "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
//                                    + msg, Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }*/



}
